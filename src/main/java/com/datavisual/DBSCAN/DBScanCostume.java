/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.DBSCAN;

import com.datavisual.model.DataSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Centry
 */
public class DBScanCostume {
    private ArrayList<DataSet> dataset = new ArrayList<>();
    private Map<String,Integer> clusterMap = new HashMap<>();

    int isian = 0;

    int posisi = 0;
    int cluster = 0;

    int temp[];
    double distance = 1;

    public void initData(ArrayList<DataSet> dataset) {
        temp = new int[dataset.size()];    
        for (int i = 0; i < dataset.size(); i++) {
            clusterMap.put(dataset.get(i).getKeyword(), 0);
        }
        this.dataset = dataset;
    }

    private static double countDistance(double x1, double y1, double x2, double y2) {
        double dx = x2 - x1;
        double dy = y2 - y1;
        double distance = Math.sqrt((Math.pow(dx, 2)) + (Math.pow(dy, 2)));

        return distance;
    }

    public  Map<String, Integer> countCluster() {
        cluster = 1;
        posisi = 0;
        temp[0] = 0;

        int hasnext = 1, nonext = 2, uppertemp = 3;
        int state = nonext;

        for (int i = 0; i < dataset.size(); i++) {

            System.out.println("ke" + i);
            if (i == 0) {

                clusterMap.put(dataset.get(i).getKeyword(), cluster);
                temp[posisi] = i;

                while (true) {
                    for (int j = 0; j < dataset.size(); j++) {
                        System.out.println(
                                "distance " + dataset.get(temp[posisi]).getAvgPosition() + "," + dataset.get(temp[posisi]).getAvgImpresion() + " dan " 
                                + dataset.get(j).getAvgPosition() + "," + dataset.get(j).getAvgImpresion()
                                + "=" + countDistance(dataset.get(temp[posisi]).getAvgPosition(), dataset.get(temp[posisi]).getAvgImpresion(),dataset.get(j).getAvgPosition(),dataset.get(j).getAvgImpresion())
                        );
                        if (
                                countDistance(dataset.get(temp[posisi]).getAvgPosition(), dataset.get(temp[posisi]).getAvgImpresion(),dataset.get(j).getAvgPosition(),dataset.get(j).getAvgImpresion()) < distance
                                && clusterMap.get(dataset.get(j).getKeyword()) == 0 && clusterMap.get(dataset.get(temp[posisi]).getKeyword()) != 0 && temp[posisi] != j) {
                            System.out.println("masuk");
                            clusterMap.put(dataset.get(j).getKeyword(),cluster);
                            System.out.println("clustered " + cluster);
                            isian = j;
                            state = hasnext;
                            break;
                        } else {
                            System.out.println("tidak masuk");
                        }

                        System.out.println("nonext");
                        state = nonext;

                    }

                    if (state == hasnext) {
                        System.out.println("lanjut");
                        posisi++;
                        temp[posisi] = isian;
                        continue;
                    }

                    if (state == nonext && posisi != 0) {
                        System.out.println("naik");
                        posisi--;
                        continue;
                    }

                    if (posisi == 0) {
                        posisi = 0;
                        cluster++;
                        break;
                    }
                }
                System.out.println("selesai");

            }
        }
        viewResult();
        return clusterMap;
    }

    private void viewResult() {
//        for (int i = 0; i < clust.length; i++) {
//            System.out.println("clust[" + i + "] = " + clust[i]
//            );
//        }
        for (Map.Entry<String, Integer> entry : clusterMap.entrySet()) {
//            Object key = entry.getKey();
//            Object val = entry.getValue();
            System.out.println("Keyword = "+entry.getKey());
            System.out.println("cluster = "+ entry.getValue());
            System.out.println("---------------------------------------");
        }
    }

//    public static void main(String[] args) {
//        TestDBScan db1 = new TestDBScan();
//        db1.init();
//        db1.cluster();
//        db1.tulis();
//
//    }
}
