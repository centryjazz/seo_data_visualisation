/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.external;

import com.datavisual.model.GoogleConsole;
import com.datavisual.model.Domain;
import com.datavisual.repository.DomainRepository;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.webmasters.Webmasters;
import com.google.api.services.webmasters.WebmastersScopes;
import com.google.api.services.webmasters.model.ApiDataRow;
import com.google.api.services.webmasters.model.SearchAnalyticsQueryRequest;
import com.google.api.services.webmasters.model.SearchAnalyticsQueryResponse;
import com.google.api.services.webmasters.model.SitesListResponse;
import com.google.api.services.webmasters.model.WmxSite;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.web.bind.annotation.RestController;

//@EnableJpaRepositories("com.datavisual.repository")
//@RestController
public class GoogleConsoleAPI {

    private final String KEYPATH = "src/main/java/com/datavisual/external/apiservicekey.json";
    
    @Autowired
    private DomainRepository domainRepository;
    
    private List<Domain> domains;

    
    public ArrayList<GoogleConsole> getData() {
        ArrayList<GoogleConsole> googleConsoles = new ArrayList<>();
        //geting all data from domainlist
        try {
            domains = domainRepository.getDomainsDataById((long)2);

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            List<String> scopes = Collections.singletonList(WebmastersScopes.WEBMASTERS_READONLY);
            GoogleCredential credentials = null;

            credentials = GoogleCredential
                    .fromStream(new FileInputStream(KEYPATH))
                    .createScoped(scopes);

            SearchAnalyticsQueryRequest queryRequest = new SearchAnalyticsQueryRequest();
            queryRequest.setSearchType("web"); // override 'web' filter default
            
            queryRequest.setStartDate("2020-01-01");
            queryRequest.setEndDate("2020-05-20");

            List<String> dimensions = Arrays.asList("page", "query", "country", "device", "date");
            queryRequest.setDimensions(dimensions);
            HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            JsonFactory jsonFactory = new JacksonFactory();
            Webmasters service = new Webmasters.Builder(httpTransport, jsonFactory, credentials)
                    .setApplicationName("webconsoleapi")
                    .build();

            Webmasters.Sites.List siteListRequest = service.sites().list();

            SitesListResponse siteListResponse = siteListRequest.execute();
            for (WmxSite site : siteListResponse.getSiteEntry()) {

                String url = site.getSiteUrl();
                long domainId = 0;

                //checking domain
                //if not found create new one
                if (searchIndex(url) != -1) {
                    domainId = searchIndex(url);
                } else {
                    Domain d = new Domain(-1, url, 1);
                    domainRepository.save(d);
                    domains.clear();
                    domains = domainRepository.findAll();
                    domainId = searchIndex(url);

                }
                Webmasters.Searchanalytics.Query query = service.searchanalytics().query(url, queryRequest);
                
                SearchAnalyticsQueryResponse queryResponse = query.execute();

                // Print site header
                System.out.println("----[ " + url + " ]----");
                // Print full JSON response
//            System.out.println(queryResponse.toPrettyString());
                if (queryResponse.getRows() == null) {
                    System.out.println("No rows returned in response");
                    return null;
                } else {

                    int idx = 0;
                    for (ApiDataRow row : queryResponse.getRows()) {
                        GoogleConsole googleConsole = new GoogleConsole(
                                idx,
                                getDate(row.getKeys().get(4)),
                                row.getKeys().get(0),
                                row.getKeys().get(1),
                                row.getKeys().get(2),
                                row.getKeys().get(3),
                                row.getClicks(),
                                row.getCtr(),
                                row.getImpressions(),
                                row.getPosition(),
                                domainId
                        );

//                    System.out.println(row);
                        idx++;
                        googleConsoles.add(googleConsole);
                        // Print an individual data point
//                    System.out.println(row.getClicks().intValue());
                    }

                }
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(GoogleConsoleAPI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException io) {
            io.printStackTrace();
        } catch (GeneralSecurityException general) {
            general.printStackTrace();
        }
        return googleConsoles;
    }

    private Date getDate(String dateStr) {
        return Date.valueOf(dateStr);
    }

//    public static void main(String[] args) {
//        GoogleConsoleAPI googleConsoleAPI = new GoogleConsoleAPI();
//        ArrayList<GoogleConsole> googleConsoles = googleConsoleAPI.getData();
//        System.out.println("data SIZE => " + googleConsoles.size());
//        
//    }

    private long searchIndex(String domain) {

        for (int i = 0; i < domains.size(); i++) {
            if (domain.equals(domains.get(i))) {
                return i;
            }
        }

        return -1;
    }

    public void setDomains(List<Domain> domains) {
        this.domains = domains;
    }
    
}
