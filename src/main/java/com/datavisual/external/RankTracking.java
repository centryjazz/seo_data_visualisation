/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.external;

//import com.sun.org.apache.xpath.internal.compiler.Keywords;
import com.datavisual.external.ApiData;
import com.datavisual.DatavisualApplication;
import com.datavisual.external.model.RankTrackingData;
import com.datavisual.external.model.RankTrackingResult;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
//import org.opencv.calib3d.Calib3d;

/**
 *
 * @author Centry
 */
public class RankTracking {

    private String DATE_FORMAT = "dd-MM-yyyy HH:mm:ss";

    private final ApiData apiData = new ApiData();
    ArrayList<RankTrackingResult> datas = new ArrayList<>();

    private final int PAGE = 1;

    public ArrayList<RankTrackingResult> getData(String keywords) throws ParseException {
        datas.clear();
        try {
            for (int i = 1; i <= PAGE; i++) {
                try {
                    // test data API
                    RankTrackingResult data = new RankTrackingResult();
                    URL url = new URL(apiData.getApiLinkScaleSerp() + apiData.getApiKeyScaleSerp() + "&q=" + keywords + "&page=" + i);
                    try {
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("GET");
                        conn.connect();
                        int responsecode = conn.getResponseCode();

                        String inline = "";
                        if (responsecode != 200) {
                            throw new RuntimeException("HttpResponseCode: " + responsecode);
                        } else {

                            Scanner sc = new Scanner(url.openStream());
                            while (sc.hasNext()) {
                                inline += sc.nextLine();
                            }
//                            System.out.println("JSON data in string format");
//                            System.out.println(inline);
                            sc.close();
                            JSONParser parser = new JSONParser();
                            JSONObject jobjct = (JSONObject) parser.parse(inline);
                            JSONObject info = (JSONObject) jobjct.get("search_information");
                            JSONArray originResult = (JSONArray) jobjct.get("organic_results");

                            //save information
                            data.setTotalResult((Long)info.get("total_results"));
                            data.setResultTime(new SimpleDateFormat(DATE_FORMAT).parse(getCurentTime()));
//                            data.setIdWebPage(webPageId);
//                  save result
                            ArrayList<RankTrackingData> rankTrackingDatas = new ArrayList<>();
                            for (int j = 0; j < originResult.size(); j++) {
                                //Store the JSON objects in an array
                                //Get the index of the JSON object and print the values as per the index
                                JSONObject getOrigin = (JSONObject) originResult.get(j);
//                                RankTrackingData da =  new RankTrackingData();
                                RankTrackingData rankData = new RankTrackingData(
                                        (Integer.parseInt(getOrigin.get("position").toString()) + (originResult.size() * (i-1))),
                                        getOrigin.get("title").toString(),
                                        getOrigin.get("link").toString(),
                                        getOrigin.get("domain").toString(),
                                        getOrigin.get("snippet").toString()
                                );
                                rankTrackingDatas.add(rankData);
                            }
                            data.setRankTrackingData(rankTrackingDatas);
                        }

                    } catch (IOException ex) {
                        Logger.getLogger(DatavisualApplication.class.getName()).log(Level.ALL.SEVERE, null, ex);
                    } catch (java.text.ParseException ex) {
                        Logger.getLogger(RankTracking.class.getName()).log(Level.SEVERE, null, ex);
                        System.out.println("Date Pharse FAILED !");
                    }
                    datas.add(data);
                    System.out.println("Rank Tracking "+ " Keyword = "+keywords+"Done......");
                } catch (Exception e) {
                    System.out.println("Data ended at page = " + i);
                    e.printStackTrace();
                    break;

                }
            }

        } catch (Exception ex) {
            Logger.getLogger(DatavisualApplication.class.getName()).log(Level.SEVERE, null, ex);
        }

        return datas;
    }

    public int getRank(){
        
        return 0;
    }
    private String getCurentTime() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_FORMAT);
        LocalDateTime now = LocalDateTime.now();

        return dtf.format(now);

    }
}
