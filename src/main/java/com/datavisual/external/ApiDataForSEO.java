/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.external;

/**
 *
 * @author Centry
 */
import com.datavisual.model.KeyWordPlaner;
import com.datavisual.model.KeyWordPlanerMonthSearch;
import com.datavisual.model.KeyWordPlannerData;
import com.google.api.client.util.Base64;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.JSONObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.aspectj.util.LangUtil;

//import org.json.simple.parser.JSONParser;
public class ApiDataForSEO {

    private final ApiData apiData = new ApiData();

//    public static void main(String[] args) {
//
//        try {
//            List keyword = Arrays.asList("derek mobil terdekat", "cara membuat kue");
//            ArrayList<KeyWordPlannerData> keyWordPlannerData = new ApiDataForSEO().getDataPlanner(keyword);
//
//            for (int i = 0; i < keyWordPlannerData.size(); i++) {
//                System.out.println("##############################");
//                System.out.println("id = "+ keyWordPlannerData.get(i).getKeyWordPlaner().getId());
//                System.out.println("keyword = " + keyWordPlannerData.get(i).getKeyWordPlaner().getKeyword());
//                System.out.println("cpc = " + keyWordPlannerData.get(i).getKeyWordPlaner().getCpc());
//                System.out.println("competition = " + keyWordPlannerData.get(i).getKeyWordPlaner().getCompetition());
//                System.out.println("volume = " + keyWordPlannerData.get(i).getKeyWordPlaner().getVolume());
//                for (int j = 0; j < keyWordPlannerData.get(i).getKeyWordPlanerMonthSearch().size(); j++) {
//                    System.out.println("-------------------------------------------------------------------------");
//                    System.out.println("id = " + keyWordPlannerData.get(i).getKeyWordPlanerMonthSearch().get(j).getId());
//                    System.out.println("year = " + keyWordPlannerData.get(i).getKeyWordPlanerMonthSearch().get(j).getYear());
//                    System.out.println("month = " + keyWordPlannerData.get(i).getKeyWordPlanerMonthSearch().get(j).getMonth());
//                    System.out.println("search volume = " + keyWordPlannerData.get(i).getKeyWordPlanerMonthSearch().get(j).getSearch_volume());
//                    System.out.println("***************************");
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
    public ArrayList<KeyWordPlannerData> getDataPlanner(List<String> keyword, long idDomain) {

        ArrayList<KeyWordPlannerData> keyWordPlannerDatas = new ArrayList<>();
        try {

            String query_url = apiData.getApiDataForSeoURL();
            //creating json array data for keyword list
            String key = "";
            for (int i = 0; i < keyword.size(); i++) {
                key = key + "\"" + keyword.get(i) + "\"";
                if (i != (keyword.size() - 1)) {
                    key = key + ", ";
                }

            }

//            String st = "\"adada\"";
            String json = "[{ \"location_code\" : 2360,\"location_name\":\"Indonesia\" ,\"keywords\" : [ " + key + "] }]";

            String auth = apiData.getApiDataForSeoUsername() + ":" + apiData.getApiDataForSeoPassword();
            byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(StandardCharsets.UTF_8));
            String authHeaderValue = "Basic " + new String(encodedAuth);
            try {
                URL url = new URL(query_url);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setConnectTimeout(5000);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Authorization", authHeaderValue);
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestMethod("POST");
                OutputStream os = conn.getOutputStream();
                os.write(json.getBytes("UTF-8"));
                os.close();
                // read the response

                //-------------
                InputStream in = new BufferedInputStream(conn.getInputStream());

                String result = IOUtils.toString(in, "UTF-8");
//                System.out.println(result);

                JSONObject myResponse = new JSONObject(result);

                String jsonString = myResponse.toString();

                JsonObject jsonObject = new JsonParser().parse(jsonString).getAsJsonObject();

                JsonArray arr = jsonObject.getAsJsonArray("tasks");

                String keyWord = "";
                double cpc = 0;
                int searchVolume = 0;
                double competition = 0;
                int historyYear = 0;
                int historyMonth = 0;
                int historySearchVolume = 0;

                for (int i = 0; i < arr.size(); i++) {
                    
                    JsonArray resultData = (JsonArray) arr.get(i).getAsJsonObject().get("result");
                    
                    for (int j = 0; j < resultData.size(); j++) {
                        KeyWordPlannerData keyWordPlannerData = new KeyWordPlannerData();
                        try {
                            keyWord = resultData.get(j).getAsJsonObject().get("keyword").getAsString();
                            cpc = Double.parseDouble(resultData.get(j).getAsJsonObject().get("cpc").getAsString());
                            searchVolume = Integer.parseInt(resultData.get(j).getAsJsonObject().get("search_volume").getAsString());
                            competition = Double.parseDouble(resultData.get(j).getAsJsonObject().get("competition").getAsString());

                            KeyWordPlaner keyWordPlaner = new KeyWordPlaner(0, keyWord, cpc, searchVolume, competition, 0, "",idDomain);
                            keyWordPlannerData.setKeyWordPlaner(keyWordPlaner);


                        } catch (Exception e) {
                            System.out.println("break loop : BAD JSON Data..");
                            e.printStackTrace();
                            break;
                            
                        }
                        
                        ArrayList<KeyWordPlanerMonthSearch> keyWordPlanerMonthSearchs = new ArrayList<>();
                        JsonArray monthResult = (JsonArray) resultData.get(j).getAsJsonObject().get("monthly_searches");
                        for (int k = 0; k < monthResult.size(); k++) {
                            historyYear = Integer.parseInt(monthResult.get(k).getAsJsonObject().get("year").getAsString());
                            historyMonth = Integer.parseInt(monthResult.get(k).getAsJsonObject().get("month").getAsString());
                            historySearchVolume = Integer.parseInt(monthResult.get(k).getAsJsonObject().get("search_volume").getAsString());
                            KeyWordPlanerMonthSearch keyWordPlanerMonthSearch = new KeyWordPlanerMonthSearch(0, historyYear, historyMonth, historySearchVolume, 0);

                            keyWordPlanerMonthSearchs.add(keyWordPlanerMonthSearch);
                        }
                        
                        
                        keyWordPlannerData.setKeyWordPlanerMonthSearch(keyWordPlanerMonthSearchs);
                        keyWordPlannerDatas.add(keyWordPlannerData);
                    }                    
                    
                }

                in.close();

                conn.disconnect();

                in.close();

                conn.disconnect();
                System.out.println("SUCCESS TO GET DATA..");
            } catch (Exception e) {
                System.out.println(e);
                e.printStackTrace();
                System.out.println("FAILED TO GET DATA");
            }

        } catch (Exception e) {

        }

        return keyWordPlannerDatas;
    }

    private String generateId() {
        Date date = (Date) Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
        String name = dateFormat.format(date);
        return name;
    }
}
