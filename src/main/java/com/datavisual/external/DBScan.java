/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.external;

/**
 *
 * @author Centry
 */
public class DBScan {

    double x[];
    double y[];
    int clust[];

    int isian = 0;

    int posisi = 0;
    int cluster = 0;

    int temp[];
    double jarak = 5;

    public void init() {
        x = new double[12];
        y = new double[12];
        clust = new int[12];
        temp = new int[12];

        x[0] = 10;
        y[0] = 10;
        x[1] = 21;
        y[1] = 21;
        x[2] = 30;
        y[2] = 30;
        x[3] = 11;
        y[3] = 11;
        x[4] = 3;
        y[4] = 3;
        x[5] = 20;
        y[5] = 20;
        x[6] = 31;
        y[6] = 31;
        x[7] = 2;
        y[7] = 2;
        x[8] = 12;
        y[8] = 12;
        x[9] = 22;
        y[9] = 22;
        x[10] = 1;
        y[10] = 1;
        x[11] = 23;
        y[11] = 23;

        clust[0] = 0;
        clust[1] = 0;
        clust[2] = 0;
        clust[3] = 0;
        clust[4] = 0;
        clust[5] = 0;
        clust[6] = 0;
        clust[7] = 0;
        clust[8] = 0;
        clust[9] = 0;

    }

    static double hitJarak(double x1, double y1, double x2, double y2) {
        double dx = x2 - x1;
        double dy = y2 - y1;
        double jarak = Math.sqrt((Math.pow(dx, 2)) + (Math.pow(dy, 2)));

        return jarak;
    }

    public void cluster() {
        cluster = 1;
        posisi = 0;
        temp[0] = 0;

        int hasnext = 1, nonext = 2, uppertemp = 3;
        int state = nonext;

        for (int i = 0; i < x.length; i++) {

            System.out.println("iteration - " + i
            );

            if (clust[i] == 0) {

                clust[i] = cluster; //kode gila
                temp[posisi] = i;  //kode gila

                while (true) {
                    for (int j = 0; j < x.length; j++) {
                        System.out.println("distance " + x[temp[posisi]] + "," + y[temp[posisi]] + " and " + x[j] + "," + y[j]
                                + "=" + hitJarak(x[temp[posisi]], y[temp[posisi]], x[j], y[j])
                        );
                        if (hitJarak(x[temp[posisi]], y[temp[posisi]], x[j], y[j]) < jarak && clust[j] == 0 && clust[temp[posisi]] != 0 && temp[posisi] != j) {
                            System.out.println("found");
                            clust[j] = cluster;
                            System.out.println("clustered " + cluster
                            );
                            isian = j;

                            state = hasnext;
                            break;

                        } else {
                            System.out.println("un found");
                        }

                        System.out.println("no next");
                        state = nonext;

                    }

                    if (state == hasnext) {
                        System.out.println(
                                "continue");
                        posisi++;
                        temp[posisi] = isian;
                        continue;
                    }

                    if (state == nonext && posisi != 0) {
                        System.out.println("up");
                        posisi--;
                        continue;
                    }

                    if (posisi == 0) {

                        posisi = 0;
                        cluster++;
                        break;
                    }
                }
                System.out.println("done");

            }
        }

    }

    public void printResult() {
        for (int i = 0; i < clust.length; i++) {
            System.out.println("cluster[" + i + "] = " + clust[i]
            );
        }
    }

    public static void main(String[] args) {
        DBScan db1 = new DBScan();
        db1.init();
        db1.cluster();
        System.out.println("================= RESULT ==============");
        System.out.println("");
        db1.printResult();

    }

}
