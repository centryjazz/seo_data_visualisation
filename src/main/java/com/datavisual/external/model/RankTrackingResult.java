/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.external.model;

import java.util.ArrayList;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Centry
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RankTrackingResult {
    private Long totalResult;
    private long idWebPage;
    private Date resultTime;
    private  ArrayList<RankTrackingData>rankTrackingData;

}
