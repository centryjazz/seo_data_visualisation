/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.controller;

import com.datavisual.model.AuthLogin;
import com.datavisual.repository.AuthLoginRepository;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.datavisual.SettingFile;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 *
 * @author Centry
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/auth")
public class AuthLoginController {

    @Autowired
    private AuthLoginRepository authLoginRepository;

    @PostMapping("/")
    public Map<String,Long> getLogin(@RequestBody AuthLogin data) {
        Map<String, Long> status = new HashMap<>();
        status.put("Status", (long)-1);
        int exists = authLoginRepository.checkUser(data.getUsername());
        AuthLogin user = authLoginRepository.getAuth(data.getUsername());

        if (exists > 0) {
            if (data.getPassword().equals(user.getPassword())) {
                status.put("Status", (long)1);
                status.put("id",user.getId());
            }
        }
//        System.out.println("status pass > "+ user.getPassword());
//        System.out.println(user.toString());
//        System.out.println("data pass > "+data.getPassword());/

        return status;

    }

}
