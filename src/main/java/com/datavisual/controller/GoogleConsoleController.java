/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.controller;

import com.datavisual.external.GoogleConsoleAPI;
import com.datavisual.model.GoogleConsole;
import com.datavisual.model.Domain;
import com.datavisual.model.Log;
import com.datavisual.repository.DomainRepository;
import com.datavisual.repository.GoogleConsoleRepository;
import com.datavisual.repository.LogRepository;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.webmasters.Webmasters;
import com.google.api.services.webmasters.WebmastersScopes;
import com.google.api.services.webmasters.model.ApiDataRow;
import com.google.api.services.webmasters.model.SearchAnalyticsQueryRequest;
import com.google.api.services.webmasters.model.SearchAnalyticsQueryResponse;
import com.google.api.services.webmasters.model.SitesListResponse;
import com.google.api.services.webmasters.model.WmxSite;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Centry
 */
@CrossOrigin
@RestController
@RequestMapping("/googleconsole")
public class GoogleConsoleController {

    @Autowired
    private GoogleConsoleRepository googleConsoleRepository;

    @Autowired
    private DomainRepository domainRepository;

    @Autowired
    private LogRepository logRepository;

    private final String KEYPATH = "src/main/java/com/datavisual/external/apiservicekey.json";
    private List<Domain> domains;

    @GetMapping("/removeall")
    public Map<String, Boolean> removeAll() {
        googleConsoleRepository.deleteAll();
        Map<String, Boolean> status = null;
        status.put("Delete All", true);
        return status;

    }

    @GetMapping("/update")
    public Map<String, String> updateData() {

        Map<String, String> response = new HashMap<>();
        ArrayList<GoogleConsole> googleConsoles = getData();
//        googleConsoles = 
        googleConsoleRepository.deleteAll();

        Log statusLog = null;
        statusLog = new Log(0, getCurentTime(), "Updating Google Console Data", true);

//        System.out.println("SIZE google console > " + googleConsoles.size());
//        System.out.println(" data :> " + googleConsoles.get(0).toString());
        if (googleConsoles.isEmpty()) {
            response.put("Status", "Failed");
            response.put("Message", "Empty Data");
            statusLog.setStatus(false);
            logRepository.save(statusLog);
            return response;
        } else {
//            for (int i = 0; i < googleConsoles.size(); i++) {
            googleConsoleRepository.saveAll(googleConsoles);
//            }
            response.put("Status", "Succesful");
            statusLog.setStatus(true);
            logRepository.save(statusLog);
            return response;
        }

    }

    private ArrayList<GoogleConsole> getData() {
        ArrayList<GoogleConsole> googleConsoles = new ArrayList<>();
        //geting all data from domainlist
        try {
            domains = domainRepository.findAll();

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            List<String> scopes = Collections.singletonList(WebmastersScopes.WEBMASTERS_READONLY);
            GoogleCredential credentials = null;

            credentials = GoogleCredential
                    .fromStream(new FileInputStream(KEYPATH))
                    .createScoped(scopes);

            SearchAnalyticsQueryRequest queryRequest = new SearchAnalyticsQueryRequest();
            queryRequest.setSearchType("web"); // override 'web' filter default
            queryRequest.setStartDate("2020-01-01");
            queryRequest.setEndDate("2020-12-30");

            List<String> dimensions = Arrays.asList("page", "query", "country", "device", "date");
            queryRequest.setDimensions(dimensions);
            HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            JsonFactory jsonFactory = new JacksonFactory();
            Webmasters service = new Webmasters.Builder(httpTransport, jsonFactory, credentials)
                    .setApplicationName("webconsoleapi")
                    .build();

            Webmasters.Sites.List siteListRequest = service.sites().list();

            SitesListResponse siteListResponse = siteListRequest.execute();
            for (WmxSite site : siteListResponse.getSiteEntry()) {

                String url = site.getSiteUrl();
                long domainId = 0;

                //checking domain
                //if not found create new one
                if (searchIndex(url) != -1) {
                    domainId = domains.get((int) searchIndex(url)).getId();;
                } else {
                    Domain d = new Domain(-1, url, 1);
                    domainRepository.save(d);
                    domains.clear();
                    domains = domainRepository.findAll();
                    domainId = domains.get((int) searchIndex(url)).getId();

                }
                Webmasters.Searchanalytics.Query query = service.searchanalytics().query(url, queryRequest);

                SearchAnalyticsQueryResponse queryResponse = query.execute();

                // Print site header
                System.out.println("----[ " + url + " ]----");
                // Print full JSON response
//                System.out.println(queryResponse.toPrettyString());
                if (queryResponse.getRows() == null) {
                    System.out.println("No rows returned in response");
                    return null;
                } else {

                    long idx = 0;
                    for (ApiDataRow row : queryResponse.getRows()) {
                        GoogleConsole googleConsole = new GoogleConsole(
                                idx,
                                getDate(row.getKeys().get(4)),
                                row.getKeys().get(0),
                                row.getKeys().get(1),
                                row.getKeys().get(2),
                                row.getKeys().get(3),
                                row.getClicks(),
                                row.getCtr(),
                                row.getImpressions(),
                                row.getPosition(),
                                domainId
                        );

//                    System.out.println(row);
                        idx++;
                        googleConsoles.add(googleConsole);
                        // Print an individual data point
//                    System.out.println(row.getClicks().intValue());
                    }

                }
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(GoogleConsoleAPI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException io) {
            io.printStackTrace();
        } catch (GeneralSecurityException general) {
            general.printStackTrace();
        }
        return googleConsoles;
    }

    private Date getDate(String dateStr) {
        return Date.valueOf(dateStr);
    }

    private long searchIndex(String domain) {

        for (int i = 0; i < domains.size(); i++) {
            if (domain.equals(domains.get(i).getDomain())) {
                return i;
            }
        }

        return -1;
    }

    public void setDomains(List<Domain> domains) {
        this.domains = domains;
    }

    private Timestamp getCurentTime() {
        java.util.Date date = new java.util.Date();
        java.sql.Timestamp sqlTime = new java.sql.Timestamp(date.getTime());
        return sqlTime;

    }
}
