/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.controller;

import com.datavisual.model.BackLinkSite;
import com.datavisual.model.BackLinkSumary;
import com.datavisual.model.BackLinkTarget;
import com.datavisual.model.BackLinkText;
import com.datavisual.model.Test;
import com.datavisual.repository.BackLinkSiteRepository;
import com.datavisual.repository.BackLinkTargetRepository;
import com.datavisual.repository.BackLinkTextRepository;
import com.opencsv.CSVReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Centry
 */
@CrossOrigin
@RestController
@RequestMapping("/backlink")
public class BackLinkAnalystController {

    @Autowired
    private BackLinkSiteRepository backLinkSiteRepository;
    @Autowired
    private BackLinkTargetRepository backLinkTargetRepository;
    @Autowired
    private BackLinkTextRepository backLinkTextRepository;

    @GetMapping("/site/{idDomain}")
    public List<BackLinkSite> getBacklinkSite(@PathVariable(name = "idDomain") Long idDomain) {

        return backLinkSiteRepository.getBackLinkSite(idDomain);
    }

    @GetMapping("/site/update/{idDomain}")
    public Map<String, Boolean> backLinkSiteUpdate(@PathVariable(name = "idDomain") Long idDomain) {
        Map<String, Boolean> status = new HashMap<>();
        status.put("Status", Boolean.TRUE);
        try {
            List<BackLinkSite> backLinkSites = updateData(idDomain);
            List<BackLinkSite> entity = backLinkSiteRepository.getBackLinkSite(idDomain);
            backLinkSiteRepository.deleteAll(entity);
            backLinkSiteRepository.saveAll(backLinkSites);
            Thread.sleep(2000);
        } catch (Exception e) {
            e.printStackTrace();
            status.put("Status", Boolean.FALSE);
        }
        return status;
    }

    private List<BackLinkSite> updateData(long idDomain) {
        List<BackLinkSite> backBackLinkSites = new ArrayList<>();
        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader("csv/back_link_site.csv"));
        } catch (FileNotFoundException ex) {
//            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("DATA back_link_site.csv NOT FOUND !");
        }
        StringBuffer buffer = new StringBuffer();
        String line[] = null;
        Iterator it = reader.iterator();
        while (it.hasNext()) {
            line = (String[]) it.next();
            try {
                backBackLinkSites.add(
                        new BackLinkSite((long) -1, line[0], Integer.parseInt(line[1]), Integer.parseInt(line[2]), idDomain)
                );
            } catch (Exception e) {
                System.out.println("SKIPED TITLE");
            }

//            System.out.println(Arrays.toString(line));
//            System.out.println(" ");
        }
        return backBackLinkSites;
    }

    @GetMapping("/targetpage/{idDomain}")
    public List<BackLinkTarget> getBackLinkTarget(@PathVariable (value = "idDomain") Long idDomain) {
        return backLinkTargetRepository.getBackLinkTarget(idDomain);
    }

    @GetMapping("/targetpage/update/{idDomain}")
    public Map<String, Boolean> backLinkTargetUpdate(@PathVariable(name = "idDomain") Long idDomain) {
        Map<String, Boolean> status = new HashMap<>();
        status.put("Status", Boolean.TRUE);
        try {
            List<BackLinkTarget> backLinkTargets = updateDataTarget(idDomain);            
            List<BackLinkTarget> entity = backLinkTargetRepository.getBackLinkTarget(idDomain);
            backLinkTargetRepository.deleteAll(entity);
            backLinkTargetRepository.saveAll(backLinkTargets);
            Thread.sleep(2000);
        } catch (Exception e) {
            e.printStackTrace();
            status.put("Status", Boolean.FALSE);
        }
        return status;
    }

    private List<BackLinkTarget> updateDataTarget(long idDomain) {
        List<BackLinkTarget> backLinkTargets = new ArrayList<>();
        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader("csv/target_page.csv"));
        } catch (FileNotFoundException ex) {
//            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("DATA target_page.csv NOT FOUND !");
        }
        StringBuffer buffer = new StringBuffer();
        String line[] = null;
        Iterator it = reader.iterator();
        while (it.hasNext()) {
            line = (String[]) it.next();
            try {
                backLinkTargets.add(
                        new BackLinkTarget((long) -1, line[0], Integer.parseInt(line[1]), Integer.parseInt(line[2]), idDomain)
                );
            } catch (Exception e) {
                System.out.println("SKIPED TITLE");
            }
        }
        return backLinkTargets;
    }
    

    @GetMapping("/text/{idDomain}")
    public List<BackLinkText> getBackLinkText(@PathVariable(value = "idDomain")Long idDomain) {
        return backLinkTextRepository.getBackLinkText(idDomain);
    }

    @GetMapping("/text/update/{idDomain}")
    public Map<String, Boolean> backLinkTextUpdate(@PathVariable(name = "idDomain") Long idDomain) {
        Map<String, Boolean> status = new HashMap<>();
        status.put("Status", Boolean.TRUE);
        try {
            List<BackLinkText> backLinkTexts = updateDataText(idDomain);            
            List<BackLinkText> entity = backLinkTextRepository.getBackLinkText(idDomain);
            backLinkTextRepository.deleteAll(entity);
            backLinkTextRepository.saveAll(backLinkTexts);
            Thread.sleep(2000);
        } catch (Exception e) {
            e.printStackTrace();
            status.put("Status", Boolean.FALSE);
        }
        return status;
    }

    private List<BackLinkText> updateDataText(long idDomain) {
        List<BackLinkText> backLinkTexts = new ArrayList<>();
        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader("csv/back_link_text.csv"));
        } catch (FileNotFoundException ex) {
//            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("DATA back_link_text.csv NOT FOUND !");
        }
        StringBuffer buffer = new StringBuffer();
        String line[] = null;
        Iterator it = reader.iterator();
        while (it.hasNext()) {
            line = (String[]) it.next();
            try {
                backLinkTexts.add(
                        new BackLinkText((long) -1, Integer.parseInt(line[0]), line[1], idDomain)
                );
            } catch (Exception e) {
                System.out.println("SKIPED TITLE");
            }
        }
        return backLinkTexts;
    }
    
    @GetMapping("/sumary/{idDomain}")
    public BackLinkSumary getSumary(@PathVariable(name = "idDomain") Long idDomain){
        
        return new BackLinkSumary(
                backLinkSiteRepository.getTotalBackLink(idDomain),
                backLinkSiteRepository.getTotalSite(idDomain),
                backLinkTargetRepository.getTotalPageLinked(idDomain),
                backLinkTextRepository.getTotalKeyword(idDomain));
    }

}
