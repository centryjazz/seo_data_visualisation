/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.controller;

import com.datavisual.exception.ResourceNotFoundException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.datavisual.model.WebPage;
import com.datavisual.repository.WebPageRepository;
import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author Centry
 */
@RestController
@RequestMapping("/api/webpage")
public class WebPageController {

    @Autowired
    private WebPageRepository webPageRepository;

    @GetMapping("/all")
    public List<WebPage> getAllWebPage() {
        return webPageRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<WebPage> getWebPageById(@PathVariable(value = "id") Long webPageId) throws ResourceNotFoundException {
        WebPage webPage = webPageRepository.findById(webPageId).orElseThrow(() -> new ResourceNotFoundException("web page not found ! id = " + webPageId));
        return ResponseEntity.ok().body(webPage);
    }

    @PostMapping("/add")
    public WebPage addWebPage(@Valid @RequestBody WebPage webPage) {
        return webPageRepository.save(webPage);
    }
    
//    @PutMapping("/edit/{id}")
//    public ResponseEntity<WebPage> updateWebPage(@Valid @RequestBody WebPage webPageData, @PathVariable(value = "id") Long webPageId) throws ResourceNotFoundException{
//        WebPage webPage = webPageRepository.findById(webPageId).orElseThrow(() -> new ResourceNotFoundException("web page not found ! id = " + webPageId));
//        
//       webPage.setTitle(webPageData.getTitle());
//       webPage.setLink(webPageData.getLink());
//       webPage.setRank((webPageData.getRank()));
//       webPage.setLastUpdate(webPageData.getLastUpdate());
//       webPage.setStatus(webPageData.getStatus());
//       
//       final WebPage updateWebPage = webPageRepository.save(webPage);
//       return ResponseEntity.ok(updateWebPage);
//        
//    }
     @DeleteMapping("/remove/{id}")
    public Map<String, Boolean> deleteKeyWord(@PathVariable(value = "id") Long webPageId)
            throws ResourceNotFoundException {
        WebPage webPage = webPageRepository.findById(webPageId)
                .orElseThrow(() -> new ResourceNotFoundException("KeyWord not found for this id :: " + webPageId));
        webPageRepository.delete(webPage);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
    
    public List<WebPage> getAll() {
        return webPageRepository.findAll();
    }
}
