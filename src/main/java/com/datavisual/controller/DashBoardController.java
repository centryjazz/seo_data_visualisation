/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.controller;

import com.datavisual.model.DashBoardData;
import com.datavisual.repository.GoogleConsoleRepository;
import java.text.DecimalFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Centry
 */
@CrossOrigin
@RestController
@RequestMapping("/dashboard")
public class DashBoardController {
    
    @Autowired
    private GoogleConsoleRepository googleConsoleRepository;
    
    @GetMapping("/{idDomain}")
    public DashBoardData getDashboard(@PathVariable(name = "idDomain") Long idDomain){
        return new DashBoardData(
                convertDouble(googleConsoleRepository.getRank(idDomain)),
                googleConsoleRepository.getTodayTrafic(idDomain),
                googleConsoleRepository.getTotalBacklink(idDomain),
                googleConsoleRepository.getTotalTrafic(idDomain),
                googleConsoleRepository.getTotalClick(idDomain),
                convertDouble(googleConsoleRepository.getTotalCTR(idDomain)),
                googleConsoleRepository.getTotalKeyword(idDomain));
    }
    
     private double convertDouble(double d) {
        DecimalFormat df = new DecimalFormat("#.#####");
        double z = Double.parseDouble(df.format(d));
        return z;
    }
    
}
