/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.controller;


import com.datavisual.model.Domain;
import com.datavisual.repository.DomainRepository;
import com.datavisual.repository.GoogleConsoleRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Centry
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/domain")
public class DomainController {
    private final String KEYPATH = "src/main/java/com/datavisual/external/apiservicekey.json";
    
    @Autowired
    private DomainRepository domainRepository;
//    @Autowired
//    private GoogleConsoleRepository googleConsoleRepository;
    
    private List<Domain> domains;
    
//    @Autowired
//    private DomainRepository domainRepository;

    @GetMapping("/all/{id}")
    public List<Domain> getAll(@PathVariable(name = "id")Long id) {
        return domainRepository.findByUser(id);
    }
    

//    @GetMapping("/update")
//    public Map<String, String> updateData() {
//
//        Map<String, String> response = new HashMap<>();
//        ArrayList<GoogleConsole> googleConsoles = new ArrayList<>();
//        googleConsoles = getData();
//        System.out.println("SIZE google console > "+googleConsoles.size());
//        if (googleConsoles.isEmpty()) {
//            response.put("Status", "Failed");
//            response.put("Message", "Empty Data");
//            return response;
//        } else {
//            for (int i = 0; i < googleConsoles.size(); i++) {
//                googleConsoleRepository.save(googleConsoles.get(i));
//            }
//            response.put("Status", "Succesful");
//            return response;
//        }
//
//    }
    
    


}
