package com.datavisual.controller;

import com.datavisual.model.GoogleConsole;
import com.datavisual.model.KeyWordResearch;
import com.datavisual.model.KeywordResearchSumary;
import com.datavisual.repository.GoogleConsoleRepository;
import com.datavisual.repository.KeyWordResearchRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/keyword/research")
public class KeyWordResearchController {

//    @Autowired
//    private KeyWordResearchRepository keyWordResearchRepository;
//
//    @GetMapping("/all")
//    public List<KeyWordResearch> getDataKeyWordResearch() {
//        return keyWordResearchRepository.getAllDataKeywordResearch();
//    }
//
//    @GetMapping("/data")
//    public List<KeyWordResearch> getData() {
//        List<KeyWordResearch> keyWordResearchs = keyWordResearchRepository.getAllDataKeywordResearch();
//        List<String> keyword = keyWordResearchRepository.getKeyword();
//
////        System.out.println(keyWordResearchs.size());
////        System.out.println(keyword.size());
//        List<KeyWordResearch> dataStream = new ArrayList<>();
//        int found = 0;
//        for (int i = 0; i < keyword.size(); i++) {
//
//            for (int j = 0; j < keyWordResearchs.size(); j++) {
//                if (keyword.get(i).equals(keyWordResearchs.get(j).getKeyword())) {
//                    if (found == 0) {
////                        System.out.println("init");
////                        KeyWordResearch key 
//                        dataStream.add(new KeyWordResearch(
//                                keyWordResearchs.get(j).getId(),
//                                keyWordResearchs.get(j).getAdd_date(),
//                                keyWordResearchs.get(j).getKeyword(),
//                                keyWordResearchs.get(j).getClick(),
//                                keyWordResearchs.get(j).getCtr(),
//                                keyWordResearchs.get(j).getImpresion(),
//                                keyWordResearchs.get(j).getPosition()
//                        ));
//                        found++;
//                    } else {
////                        System.out.println("calculate");
//                        found++;
//                        dataStream.get(i).setClick(
//                                dataStream.get(i).getClick() + keyWordResearchs.get(j).getClick()
//                        );
//                        dataStream.get(i).setCtr(
//                                dataStream.get(i).getCtr() + keyWordResearchs.get(j).getCtr()
//                        );
//                        dataStream.get(i).setImpresion(
//                                dataStream.get(i).getImpresion() + keyWordResearchs.get(j).getImpresion()
//                        );
//                        dataStream.get(i).setPosition(
//                                (dataStream.get(i).getPosition() + keyWordResearchs.get(j).getPosition())
//                        );
//                    }
//
//                }
//                dataStream.get(i).setPosition(
//                        dataStream.get(i).getPosition() / found
//                );
//            }
//
//            found = 0;
//        }
////        System.out.println("total Data = "+dataStream.size());
//        return dataStream;
//    }
//
//    @GetMapping("/sumary")
//    public List<KeywordResearchSumary> getKeywordSumary() {
//        List<KeyWordResearch> keyWordResearchs = keyWordResearchRepository.getAllDataKeywordResearch();
//        List<String> keyword = keyWordResearchRepository.getKeyword();
//        List<KeywordResearchSumary> dataStream = new ArrayList<>();
//        int found = 0;
//
//        for (int i = 0; i < keyword.size(); i++) {
//
//            for (int j = 0; j < keyWordResearchs.size(); j++) {
//                if (keyword.get(i).equals(keyWordResearchs.get(j).getKeyword())) {
//                    if (found == 0) {
////                        System.out.println("init");
////                        KeyWordResearch key 
//                        dataStream.add(new KeywordResearchSumary(
//                                keyWordResearchs.get(j).getKeyword(),
//                                keyWordResearchs.get(j).getClick(),
//                                keyWordResearchs.get(j).getCtr(),
//                                keyWordResearchs.get(j).getImpresion(),
//                                keyWordResearchs.get(j).getPosition(),
//                                keyWordResearchs.get(j).getImpresion(),
//                                keyWordResearchs.get(j).getCtr(),
//                                keyWordResearchs.get(j).getPosition(),
//                                keyWordResearchs.get(j).getClick(),
//                                keyWordResearchs.get(j).getCtr(),
//                                keyWordResearchs.get(j).getImpresion()
//                        ));
//                        found++;
//                    } else {
////                        System.out.println("calculate");
//                        found++;
//                        dataStream.get(i).setAvgClick(
//                                dataStream.get(i).getAvgClick() + keyWordResearchs.get(j).getClick()
//                        );
//                        dataStream.get(i).setAvgCtr(
//                                dataStream.get(i).getAvgCtr() + keyWordResearchs.get(j).getCtr()
//                        );
//                        dataStream.get(i).setAvgImpresion(
//                                dataStream.get(i).getAvgImpresion() + keyWordResearchs.get(j).getImpresion()
//                        );
//                        dataStream.get(i).setAvgPosition(
//                                dataStream.get(i).getAvgPosition() + keyWordResearchs.get(j).getPosition()
//                        );
//                        dataStream.get(i).setMaxImpresion(
//                                (dataStream.get(i).getMaxImpresion() > keyWordResearchs.get(j).getImpresion()) ? dataStream.get(i).getMaxImpresion() : keyWordResearchs.get(j).getImpresion()
//                        );
//                        dataStream.get(i).setMaxCtr(
//                                (dataStream.get(i).getMaxCtr() > keyWordResearchs.get(j).getCtr()) ? dataStream.get(i).getMaxCtr() : keyWordResearchs.get(j).getCtr()
//                        );
//                        dataStream.get(i).setMaxPosition(
//                                (dataStream.get(i).getMaxPosition() < keyWordResearchs.get(j).getPosition()) ? dataStream.get(i).getMaxPosition() : keyWordResearchs.get(j).getPosition()
//                        );
//                        dataStream.get(i).setTotalClick(
//                                dataStream.get(i).getAvgClick() + keyWordResearchs.get(j).getClick()
//                        );
//                        dataStream.get(i).setTotalCtr(
//                                dataStream.get(i).getAvgCtr() + keyWordResearchs.get(j).getCtr()
//                        );
//                        dataStream.get(i).setTotalImpresion(
//                                dataStream.get(i).getAvgImpresion() + keyWordResearchs.get(j).getImpresion()
//                        );
//
//                    }
//
//                }
//            }
//            dataStream.get(i).setAvgClick(
//                    dataStream.get(i).getAvgClick() / found
//            );
//            dataStream.get(i).setAvgCtr(
//                    dataStream.get(i).getAvgCtr() / found
//            );
//            dataStream.get(i).setAvgImpresion(
//                    dataStream.get(i).getAvgImpresion() / found
//            );
//            dataStream.get(i).setAvgPosition(
//                    dataStream.get(i).getAvgPosition() / found
//            );
//            found = 0;
//        }
//        
//        Comparator<KeywordResearchSumary> comparator = Comparator.comparing(e -> e.getTotalClick());
//        dataStream.sort(comparator.reversed());
//        
//        return dataStream;
//    }
}
