/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.controller;

//import com.datavisual.DatavisualApplication;
import com.datavisual.DBSCAN.DBSCANClusterer;
import com.datavisual.DBSCAN.DBSCANClusteringException;
import com.datavisual.DBSCAN.DBScanCostume;
import com.datavisual.DBSCAN.metrics.DistanceMetricNumbers;
import com.datavisual.exception.ResourceNotFoundException;
import com.datavisual.external.ApiData;
import com.datavisual.external.ApiDataForSEO;
import com.datavisual.model.DataSet;
//import com.datavisual.external.RankTracking;
//import com.datavisual.external.model.RankTrackingData;
//import com.datavisual.external.model.RankTrackingResult;
import com.datavisual.model.GraphTraficKeyword;
//import com.datavisual.model.RankTrackingKeyword;
import com.datavisual.model.KeyWordPlaner;
import com.datavisual.model.KeyWordPlanerMonthSearch;
import com.datavisual.model.KeyWordPlannerData;
import com.datavisual.model.KeywordPlanerHistory;
//import com.datavisual.model.WebPage;
import com.datavisual.repository.KeyWordPlanerMonthSearchRepository;
import com.datavisual.repository.KeyWordPlannerRepository;
//import java.io.IOException;
import java.net.HttpURLConnection;
//import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Array;
//import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
//import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
//import java.util.logging.Level;
//import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.validation.Valid;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
//import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Centry
 */
@CrossOrigin
@RestController
@RequestMapping("/keyword/planner")
public class KeyWordPlannerController {

    @Autowired
    private KeyWordPlannerRepository keyWordPlannerRepository;
    @Autowired
    private KeyWordPlanerMonthSearchRepository keyWordPlanerMonthSearchRepository;

    @GetMapping("/all/{idDomain}")
    public List<KeyWordPlaner> getAllDataPlaner(@PathVariable(name = "idDomain") Long idDomain) {
        return keyWordPlannerRepository.getAll(idDomain);
    }

    @DeleteMapping("/remove/{idKeyword}")
    public Map<String, Boolean> deleteKeyWordPlaner(@PathVariable(value = "idKeyword") Long idKeyword)
            throws ResourceNotFoundException {
        KeyWordPlaner keyWordPlaner = keyWordPlannerRepository.findById(idKeyword)
                .orElseThrow(() -> new ResourceNotFoundException("KeyWord not found for this id :: " + idKeyword));
        keyWordPlannerRepository.delete(keyWordPlaner);
        //removing history
        List<KeyWordPlanerMonthSearch> keyWordPlanerMonthSearchs = keyWordPlanerMonthSearchRepository.getDataByKeyword(idKeyword);
        keyWordPlanerMonthSearchRepository.deleteAll(keyWordPlanerMonthSearchs);

        Map<String, Boolean> response = new HashMap<>();
        response.put("Status", Boolean.TRUE);
        return response;
    }

    @PostMapping("/add")
    public KeyWordPlaner saveKeyword(@Valid @RequestBody KeyWordPlaner keyWordPlaner) {
        return keyWordPlannerRepository.save(keyWordPlaner);
    }

    @GetMapping("/history/{idKeyword}")
    public List<KeywordPlanerHistory> getByKeywordId(@PathVariable(value = "idKeyword") Long keywordId) {
        List<KeyWordPlanerMonthSearch> history = keyWordPlanerMonthSearchRepository.getDataByKeyword(keywordId);
        List<KeywordPlanerHistory> keyPlaneHistorys = new ArrayList<>();
        //converting data
        for (int i = 0; i < history.size(); i++) {
            keyPlaneHistorys.add(
                    new KeywordPlanerHistory(
                            history.get(i).getId(),
                            history.get(i).getYear(),
                            getMonthName(history.get(i).getMonth()),
                            history.get(i).getSearch_volume(),
                            history.get(i).getKeywordPlaner())
            );
        }
        return keyPlaneHistorys;
    }

    @GetMapping("/recomendation/{keyword}")
    public List<DataSet> getListRecomendation(@PathVariable(value = "keyword") String keyword) {
        List<String> keywordList = keyWordPlannerRepository.getSameKeyword(keyword);
//        System.out.println("keyword = " +keyword);
        ArrayList<DataSet> dataSet = getAvgRankKeyword(keywordList);
        DBScanCostume dbsc = new DBScanCostume();
        dbsc.initData(dataSet);
        Map<String, Integer> clusterMap = dbsc.countCluster();

        //create count data Map Cluster
        List<DataSet> result = getDataListResult(clusterMap, dataSet);

        return result;
    }

    // breakdown result to map
    private List<DataSet> getDataListResult(Map<String, Integer> clusterMap, ArrayList<DataSet> dataset) {
        List<DataSet> cluster = new ArrayList<>();
        // counting cluster
        Map<String, Integer> map = new HashMap<>();
        for (Map.Entry<String, Integer> entry : clusterMap.entrySet()) {
            map.put(Integer.toString(entry.getValue()), entry.getValue());
        }
        // counting data per cluster
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
//            int idx = getIdxMap(dataset, entry.getKey());
//            if(idx != -1){
            cluster.add(
                    new DataSet(
                            createListKeyword(dataset, entry.getValue(), clusterMap),
                            getAVGRankPosition(dataset, entry.getValue(), clusterMap),
                            getAVGRankImpresion(dataset, entry.getValue(), clusterMap))
            );

//            }
        }

        return cluster;
    }

//    private int getIdxMap(ArrayList<DataSet> dataset, String keyword) {
//        for (int i = 0; i < dataset.size(); i++) {
//            if (dataset.get(i).getKeyword().equals(keyword)) {
//                return i;
//            }
//        }
//        return -1;
//    }
    private String createListKeyword(List<DataSet> dataset, int searchingCluster, Map<String, Integer> clusterMap) {
        String st = "";
        for (Map.Entry<String, Integer> entry : clusterMap.entrySet()) {
            if (entry.getValue() == searchingCluster) {
                for (int i = 0; i < dataset.size(); i++) {
                    if (dataset.get(i).getKeyword().equals(entry.getKey())) {
                        st += dataset.get(i).getKeyword() + ", ";
                    }
                }
            }
        }
        System.out.println("List Keyword = " + st);
        return st;
    }

    private double getAVGRankPosition(List<DataSet> dataset, int searchingCluster, Map<String, Integer> clusterMap) {
        double totalAVG = 0;
        double count = 0;
        for (Map.Entry<String, Integer> entry : clusterMap.entrySet()) {
            if (entry.getValue() == searchingCluster) {
                for (int i = 0; i < dataset.size(); i++) {
                    if (dataset.get(i).getKeyword().equals(entry.getKey())) {
                        totalAVG += dataset.get(i).getAvgPosition();

                    }
                }
                count++;
            }
        }
        System.out.println("Total AVG Position = " + (totalAVG / count));
        return totalAVG / count;
    }

    private double getAVGRankImpresion(List<DataSet> dataset, int searchingCluster, Map<String, Integer> clusterMap) {
        double totalAVG = 0;
        double count = 0;
        for (Map.Entry<String, Integer> entry : clusterMap.entrySet()) {
            if (entry.getValue() == searchingCluster) {
                for (int i = 0; i < dataset.size(); i++) {
                    if (dataset.get(i).getKeyword().equals(entry.getKey())) {
                        totalAVG += dataset.get(i).getAvgImpresion();
                    }
                }
                count++;
            }
        }
        System.out.println("Total AVG Position = " + (totalAVG / count));

        return totalAVG / count;
    }

    //search avg rank for keyword
    private ArrayList<DataSet> getAvgRankKeyword(List<String> keyword) {
        ArrayList<DataSet> dataList = new ArrayList<>();
        for (int i = 0; i < keyword.size(); i++) {
            dataList.add(new DataSet(
                    keyword.get(i),
                    keyWordPlannerRepository.getAVGKeyWordPosition(keyword.get(i)),
                    keyWordPlannerRepository.getAVGKeyWordImpresion(keyword.get(i))
            ));
            System.out.println("keyword = " + keyword.get(i));
            System.out.println("AVG Position  = " + dataList.get(i).getAvgPosition());
            System.out.println("AVG Impresion = " + dataList.get(i).getAvgImpresion());
            System.out.println("-----------------------------------");
        }
        return dataList;
    }

    @GetMapping("/history/graph/{idKeyword}")
    public GraphTraficKeyword getGraphForHistoryPlanner(@PathVariable(value = "idKeyword") Long keywordId) {
        List<KeyWordPlanerMonthSearch> history = keyWordPlanerMonthSearchRepository.getDataByKeywordASC(keywordId);
        System.out.println("history size" + history.size());
        //geting listDate 2 year
        List<LocalDate> listDays = getDatesBetween(LocalDate.now().minusDays(729), LocalDate.now().plusDays(1));
        System.out.println("list day > " + listDays.size());
        List<String> monthList = new ArrayList<>();
//        geting data list month
        for (int i = 0; i < listDays.size(); i++) {

            if (checkDuplicateMonthYear(monthList, listDays.get(i).toString())) {
                monthList.add(listDays.get(i).toString().substring(0, 7));
            }
        }
        //initial data date
        String dates[] = new String[monthList.size()];
        for (int i = 0; i < monthList.size(); i++) {
            dates[i] = monthList.get(i).substring(0, 7);

        }
        // get data value
        double value[] = new double[dates.length];
        for (int i = 0; i < value.length; i++) {
            value[i] = getValue(history, dates[i]);
        }

//        converting date
        for (int i = 0; i < dates.length; i++) {
            dates[i] = dates[i].substring(0, 4) + "/" + getMonthName(Integer.parseInt(dates[i].substring(5, 7)));
        }
        return new GraphTraficKeyword(dates, value, getMaxValue(value));
    }

    private int getMaxValue(double value[]) {
        double max = 0;
        for (int i = 0; i < value.length; i++) {
            if (value[i] > max) {
                max = value[i];
            }
        }
        return (int) (max + 50);
    }

    private int getValue(List<KeyWordPlanerMonthSearch> history, String yearMonth) {
        for (int i = 0; i < history.size(); i++) {
            String month = Integer.toString(history.get(i).getMonth());
            if (month.length() == 1) {
                month = "0" + month;
            }
            if ((history.get(i).getYear() + "-" + month).equals(yearMonth)) {
                return history.get(i).getSearch_volume();
            }
        }
        return 0;
    }

    private boolean checkDuplicateMonthYear(List<String> monthList, String dataSearch) {
        try {
            for (int i = 0; i < monthList.size(); i++) {
                if (monthList.get(i).substring(0, 7).equals(dataSearch.substring(0, 7))) {
                    return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    private List<LocalDate> getDatesBetween(
            LocalDate startDate, LocalDate endDate) {
        long numOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate);
        return IntStream.iterate(0, i -> i + 1)
                .limit(numOfDaysBetween)
                .mapToObj(i -> startDate.plusDays(i))
                .collect(Collectors.toList());
    }

    private String getMonthName(int idx) {
        String name = "";
        switch (idx) {
            case 1:
                name = "January";
                break;
            case 2:
                name = "February";
                break;
            case 3:
                name = "March";
                break;
            case 4:
                name = "April";
                break;
            case 5:
                name = "May";
                break;
            case 6:
                name = "June";
                break;
            case 7:
                name = "July";
                break;
            case 8:
                name = "August";
                break;
            case 9:
                name = "September";
                break;
            case 10:
                name = "October";
                break;
            case 11:
                name = "November";
                break;
            case 12:
                name = "December";
                break;
        }
        return name;
    }

    private void printDeepData(ArrayList<KeyWordPlannerData> keyWordPlannerData) {

        for (int i = 0; i < keyWordPlannerData.size(); i++) {
            System.out.println("BASE DATA = " + keyWordPlannerData.get(i).getKeyWordPlaner().toString());
            System.out.println("History Data ");
            for (int j = 0; j < keyWordPlannerData.get(i).getKeyWordPlanerMonthSearch().size(); j++) {
                System.out.println("###################");
                System.out.println(keyWordPlannerData.get(i).getKeyWordPlanerMonthSearch().get(j).toString());
            }
        }

    }

    @GetMapping("/update/{idDomain}")
    public Map<String, Boolean> updateDataKeyPlaner(@PathVariable(name = "idDomain") Long idDomain) {

        Map<String, Boolean> status = new HashMap<>();
        status.put("Status", Boolean.TRUE);

        try {

            ArrayList<KeyWordPlannerData> keyWordPlannerData = new ApiDataForSEO().getDataPlanner(keyWordPlannerRepository.getAllKeyword(idDomain), idDomain);
            List<KeyWordPlaner> keyWordPlaners = keyWordPlannerRepository.getAll(idDomain);

//            printDeepData(keyWordPlannerData);
            //deleting curent history keyword
            for (int i = 0; i < keyWordPlaners.size(); i++) {
                List<KeyWordPlanerMonthSearch> keyWordPlanerMonthSearchs = keyWordPlanerMonthSearchRepository.getDataByKeyword(keyWordPlaners.get(i).getId());
                keyWordPlanerMonthSearchRepository.deleteAll(keyWordPlanerMonthSearchs);
            }

            //updating data from data SEO
            for (int i = 0; i < keyWordPlaners.size(); i++) {
                keyWordPlaners.get(i).setCpc(
                        keyWordPlannerData.get(i).getKeyWordPlaner().getCpc()
                );
                keyWordPlaners.get(i).setVolume(
                        keyWordPlannerData.get(i).getKeyWordPlaner().getVolume()
                );
                keyWordPlaners.get(i).setCompetition(
                        keyWordPlannerData.get(i).getKeyWordPlaner().getCompetition()
                );
            }
            keyWordPlannerRepository.saveAll(keyWordPlaners);

            //updating data history
            for (int i = 0; i < keyWordPlaners.size(); i++) {
                List<KeyWordPlanerMonthSearch> history = keyWordPlannerData.get(i).getKeyWordPlanerMonthSearch();
                for (int j = 0; j < history.size(); j++) {
                    history.get(j).setKeywordPlaner(keyWordPlaners.get(i).getId());
                }
                keyWordPlanerMonthSearchRepository.saveAll(history);
            }

            //updating from all in search and  related search;
            for (int i = 0; i < keyWordPlaners.size(); i++) {
                keyWordPlaners.get(i).setAllinsearch(
                        getAllInSearch(keyWordPlaners.get(i).getKeyword())
                );
                keyWordPlaners.get(i).setRelated_search(
                        getRelatedSearch(keyWordPlaners.get(i).getKeyword())
                );
            }
            keyWordPlannerRepository.saveAll(keyWordPlaners);
        } catch (Exception e) {
            e.printStackTrace();
            status.put("Status", Boolean.FALSE);

        }
        return status;
    }

    private int getAllInSearch(String keyword) {
        ApiData apiData = new ApiData();
        int data = 0;
        try {

            URL url = new URL(apiData.getApiLinkScaleSerp() + apiData.getApiKeyScaleSerp() + "&q=allintitle:" + removeSpace(keyword));

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();
            int responsecode = conn.getResponseCode();

            String inline = "";
            if (responsecode != 200) {
                throw new RuntimeException("HttpResponseCode: " + responsecode);
            } else {

                Scanner sc = new Scanner(url.openStream());
                while (sc.hasNext()) {
                    inline += sc.nextLine();
                }
//                            System.out.println("JSON data in string format");
//                            System.out.println(inline);
                sc.close();
                JSONParser parser = new JSONParser();
                JSONObject jobjct = (JSONObject) parser.parse(inline);
                JSONObject info = (JSONObject) jobjct.get("search_information");
                data = Integer.parseInt(info.get("total_results").toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    private String getRelatedSearch(String keyword) {
        ApiData apiData = new ApiData();
        String related = "";
        try {
            URL url = new URL(apiData.getApiLinkScaleSerp() + apiData.getApiKeyScaleSerp() + "&q=" + removeSpace(keyword));
            System.out.println("URL => " + url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();
            int responsecode = conn.getResponseCode();

            String inline = "";
            if (responsecode != 200) {
                throw new RuntimeException("HttpResponseCode: " + responsecode);
            } else {

                Scanner sc = new Scanner(url.openStream());
                while (sc.hasNext()) {
                    inline += sc.nextLine();
                }

                sc.close();
                JSONParser parser = new JSONParser();
                JSONObject jobjct = (JSONObject) parser.parse(inline);
//                JSONObject info = (JSONObject) jobjct.get("search_information");
                JSONArray relatedSearch = (JSONArray) jobjct.get("related_searches");
//                data = Integer.parseInt(info.get("total_results").toString());
                if (relatedSearch == null) {
                    return related;
                } else {
                    for (int i = 0; i < relatedSearch.size(); i++) {
                        JSONObject getOrigin = (JSONObject) relatedSearch.get(i);

                        related = related + getOrigin.get("query").toString();
                        if (i != (relatedSearch.size() - 1)) {
                            related += ", ";
                        }
                    }

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return related;
    }

    private String removeSpace(String st) {

        return st.replace(" ", "+");
    }

}
