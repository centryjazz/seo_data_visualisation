/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.controller;

import com.datavisual.DatavisualApplication;
import com.datavisual.exception.ResourceNotFoundException;
import com.datavisual.external.RankTracking;
import com.datavisual.external.model.RankTrackingResult;
import com.datavisual.model.Domain;
import com.datavisual.model.GoogleConsole;
import com.datavisual.model.GraphTraficKeyword;

import com.datavisual.model.RankTrackingKeyword;
import com.datavisual.model.Log;
import com.datavisual.model.RankTrackingLog;
import com.datavisual.model.RequestRankTrackingKeyword;
import com.datavisual.model.WebPage;
import com.datavisual.repository.DomainRepository;
import com.datavisual.repository.GoogleConsoleRepository;
import com.datavisual.repository.LogRepository;
import com.datavisual.repository.WebPageRepository;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.datavisual.repository.RankTrackingKeywordRepository;
import com.datavisual.repository.RankTrackingLogRepository;
import com.google.api.client.googleapis.apache.GoogleApacheHttpTransport;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author Centry
 */
//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin
@RestController
@RequestMapping("api/ranktracking")
public class RankTrackingController {
    
    private String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private RankTracking rankTracking = new RankTracking();
//    private ArrayList<RankTrackingResult> rankTrackingResults;
//    private LocalConnection localConnection = new LocalConnection();
    private WebPageController webPageController = new WebPageController();
    private KeyWordController keyWordController = new KeyWordController();
//    private List<WebPage> webPages;
//    private List<RankTrackingKeyword> keyWords;

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private RankTrackingLogRepository rankTrackingLogRepository;
    @Autowired
    private RankTrackingKeywordRepository rankTrackingKeywordRepository;
    @Autowired
    private DomainRepository domainRepository;
    
    @Autowired
    private GoogleConsoleRepository googleConsoleRepository;
    
    @GetMapping("/update/{domainId}")
    public Map<String, Boolean> updateData(@PathVariable(name = "domainId") Long domainId) {
        Map<String, Boolean> response = new HashMap<>();
        Domain domain = domainRepository.getDomainDataById(domainId);
        Log statusLog = null;
        
        statusLog = new Log(0, getCurentTime(), "updating RankTracking", true);
        try {
            String domainName = getCleanDomain(domain.getDomain());
            List<RankTrackingKeyword> keywords = rankTrackingKeywordRepository.getKeywordListByDomain(domainId);
            
            for (int i = 0; i < keywords.size(); i++) {
                ArrayList<RankTrackingResult> rankTrackingResults = rankTracking.getData(convertKeyword(keywords.get(i).getKeyword()));
                calculateRankFromKeyword(rankTrackingResults, domainName, keywords.get(i).getId());
                updateRankTrackingKeyword(keywords.get(i));
            }
            response.put("Update", Boolean.TRUE);
            statusLog.setStatus(true);
        } catch (Exception e) {
            e.printStackTrace();
            response.put("Update", Boolean.FALSE);
            statusLog.setStatus(false);
        }
        logRepository.save(statusLog);
        
        return response;
    }
    
    @GetMapping("/data/keyword/{idDomain}")
    public List<RankTrackingKeyword> getRankTrackingKeywords(@PathVariable(name = "idDomain") Long id) {
        return rankTrackingKeywordRepository.getKeywordListByDomain(id);
    }
    
    @GetMapping("/data/keyword/top/{idDomain}")
    public Map<String,Integer>getCountTopRank(@PathVariable(name = "idDomain")Long id){
        List<RankTrackingKeyword> baseData = rankTrackingKeywordRepository.getKeywordListByDomain(id);
        Map<String, Integer> topData = new HashMap<>();
        int count = 0;
        // searcing top 3
        for (int i = 0; i < baseData.size(); i++) {
            if(baseData.get(i).getAvg_rank()<= 3 && baseData.get(i).getAvg_rank() > 0){
                count++;
            }
        }
        topData.put("top_3", count);
        count = 0;
        // searching for top 5
        for (int i = 0; i < baseData.size(); i++) {
            if(baseData.get(i).getAvg_rank()> 3 && baseData.get(i).getAvg_rank() <= 5){
                count++;
            }
        }
        topData.put("top_5", count);
        count = 0;
        // count top 10
        for (int i = 0; i < baseData.size(); i++) {
            if(baseData.get(i).getAvg_rank()> 5 && baseData.get(i).getAvg_rank() <= 10){
                count++;
            }
        }
        topData.put("top_10", count);
        count = 0;
        return topData;
    }
    @GetMapping("/data/keyword/top/3/{idDomain}")
    public List<RankTrackingKeyword> getDataTop3 (@PathVariable(name = "idDomain")Long id){
        List<RankTrackingKeyword> baseData = rankTrackingKeywordRepository.getKeywordListByDomain(id);
        List<RankTrackingKeyword> topData = new ArrayList<>();
        
        for (int i = 0; i < baseData.size(); i++) {
            if(baseData.get(i).getAvg_rank()<= 3 && baseData.get(i).getAvg_rank() > 0){
                topData.add(baseData.get(i));
            }
        }
        return topData;
    }
    @GetMapping("/data/keyword/top/5/{idDomain}")
    public List<RankTrackingKeyword> getDataTop5 (@PathVariable(name = "idDomain")Long id){
        List<RankTrackingKeyword> baseData = rankTrackingKeywordRepository.getKeywordListByDomain(id);
        List<RankTrackingKeyword> topData = new ArrayList<>();
        
        for (int i = 0; i < baseData.size(); i++) {
            if(baseData.get(i).getAvg_rank()> 3 && baseData.get(i).getAvg_rank() <= 5){
                topData.add(baseData.get(i));
            }
        }
        return topData;
    }
    @GetMapping("/data/keyword/top/10/{idDomain}")
    public List<RankTrackingKeyword> getDataTop10 (@PathVariable(name = "idDomain")Long id){
        List<RankTrackingKeyword> baseData = rankTrackingKeywordRepository.getKeywordListByDomain(id);
        List<RankTrackingKeyword> topData = new ArrayList<>();
        
        for (int i = 0; i < baseData.size(); i++) {
            if(baseData.get(i).getAvg_rank()> 5 && baseData.get(i).getAvg_rank() <= 10){
                topData.add(baseData.get(i));
            }
        }
        return topData;
    }
    
    @GetMapping("/data/log/{id}")
    public List<RankTrackingLog> getRankTrackingLog(@PathVariable(name = "id") Long keywordId) {
        
        List<RankTrackingLog> data = rankTrackingLogRepository.getDataByKeywordId(keywordId);
        System.out.println("DATA SIZE > "+data.size());
        List<RankTrackingLog> rankTrackingLogs = new ArrayList<>();
        // shorting 
        List<String> page = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            if(searchingDistinct(rankTrackingLogs, data.get(i).getPage())){
                rankTrackingLogs.add(
                        new RankTrackingLog(
                                data.get(i).getId(),
                                data.get(i).getRank(),
                                data.get(i).getPage(),
                                data.get(i).getDate_time(),
                                data.get(i).getKeyword_id())
                );
            }
        }
        
        
        return rankTrackingLogs;
    }
    
    private boolean searchingDistinct(List<RankTrackingLog> data, String page){
        for (int i = 0; i < data.size(); i++) {
            if(page.equals(data.get(i).getPage())){
                return false;
            }
        }
        return true;
    }
    
    @GetMapping("/log")
    public List<Log> getAllLog() {
        return logRepository.findAll();
    }
    
    @PostMapping("/data/keyword/add")
    public RankTrackingKeyword saveRankTrackingKeyword(@RequestBody RankTrackingKeyword rankTrackingKeyword) {
        return rankTrackingKeywordRepository.save(rankTrackingKeyword);
    }
    
    @DeleteMapping("/data/keyword/delete/{id}")
    public Map<String, Integer> deleteKeyword(@PathVariable(value = "id") Long keyWordId)
            throws ResourceNotFoundException {
        Map<String, Integer> response = new HashMap<>();
        response.put("id", 03137);
        response.put("Status", -1);
        RankTrackingKeyword keyWord = rankTrackingKeywordRepository.findById(keyWordId)
                .orElseThrow(() -> new ResourceNotFoundException("KeyWord not found for this id :: " + keyWordId));
        
        rankTrackingKeywordRepository.delete(keyWord);
        response.put("Status", 1);
        return response;
    }
    
    @GetMapping("/data/sumary/{idDomain}")
    public Map<String, Double> getSumary(@PathVariable(name = "idDomain") Long idDomain) {
        Map<String, Double> sumary = new HashMap<>();
        
        List<RankTrackingKeyword> rankTrackingKeywords = rankTrackingKeywordRepository.getKeywordListByDomainWithValueCheck(idDomain);
        int count = 0;
        //Searching for MaxRank
        for (int i = 0; i < rankTrackingKeywords.size(); i++) {
            count += rankTrackingKeywords.get(i).getMax_rank();
        }
        sumary.put("max_rank", convertDouble(
                count / (double) rankTrackingKeywords.size())
        );
        count = 0;
        //Searching for AvgRank
        for (int i = 0; i < rankTrackingKeywords.size(); i++) {
            count += rankTrackingKeywords.get(i).getAvg_rank();
        }
        sumary.put("avg_rank", convertDouble(
                count / (double) rankTrackingKeywords.size())
        );
        count = 0;
        //Searching for MinRank
        for (int i = 0; i < rankTrackingKeywords.size(); i++) {
            count += rankTrackingKeywords.get(i).getMin_rank();
        }
        sumary.put("min_rank", convertDouble(
                count / (double) rankTrackingKeywords.size())
        );
        count = 0;

        //Searching for unranked
        sumary.put("unranked", (double) rankTrackingKeywordRepository.getUnrankedKeyword(idDomain));
        
        return sumary;
    }
    
    private void calculateRankFromKeyword(ArrayList<RankTrackingResult> rankTrackingResults, String domain, long keywordId) {
        
        try {
            for (int i = 0; i < rankTrackingResults.size(); i++) {
                for (int j = 0; j < rankTrackingResults.get(i).getRankTrackingData().size(); j++) {
                    if (rankTrackingResults.get(i).getRankTrackingData().get(j).getDomain().equals(domain)) {
                        RankTrackingLog rankTrackingLog = new RankTrackingLog(
                                -1,
                                rankTrackingResults.get(i).getRankTrackingData().get(j).getPosition(),
                                rankTrackingResults.get(i).getRankTrackingData().get(j).getLink(),
                                getCurentTime().toString(),
                                keywordId);
                        rankTrackingLogRepository.save(rankTrackingLog);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("#~> Error Calculating Rank Keyword : ");
            e.printStackTrace();
        }
    }
    
    private void updateRankTrackingKeyword(RankTrackingKeyword keyWords) {
        try {
            List<RankTrackingLog> rankTrackingLogs = rankTrackingLogRepository.getDataByKeywordId(keyWords.getId());
            if (!rankTrackingLogs.isEmpty()) {
                keyWords.setAvg_rank(avgRank(rankTrackingLogs));
                keyWords.setMax_rank(maxRank(rankTrackingLogs));
                keyWords.setMin_rank(minRank(rankTrackingLogs));
                keyWords.setLast_update(getCurentTime().toString());
                rankTrackingKeywordRepository.save(keyWords);
            } else {
                keyWords.setLast_update(getCurentTime().toString());
                rankTrackingKeywordRepository.save(keyWords);
            }
            
        } catch (Exception e) {
            System.out.println("#~> Error Updating Keyword : " + keyWords.toString());
            e.printStackTrace();
        }
    }
    
    @PostMapping("/keyword/page")
    public List<RankTrackingLog> getLogGraph(@RequestBody RequestRankTrackingKeyword requestRankTrackingKeyword){
        return rankTrackingLogRepository.getGraphForDataTraficKeyword(requestRankTrackingKeyword.getLink(), requestRankTrackingKeyword.getKeyword_id());
    }
    @PostMapping("/keyword/graph")
    public GraphTraficKeyword updateDataRankPageKeyword(@RequestBody RequestRankTrackingKeyword requestRankTrackingKeyword) {
        List<RankTrackingLog> data = rankTrackingLogRepository.getGraphForDataTraficKeyword(requestRankTrackingKeyword.getLink(), requestRankTrackingKeyword.getKeyword_id());
//        System.out.println("DATA size " + data.size());
//        System.out.println("Body data > " + requestRankTrackingKeyword.toString());
        GraphTraficKeyword graphTraficKeywords = new GraphTraficKeyword();
//        Map<String,Integer> uniqeDate = new HashMap<>();
//        for (int i = 0; i < data.size(); i++) {
//              uniqeDate.put(data.get(i).getDate().toString(), i);
//        }

        //adding base data
        List<LocalDate> listDays = getDatesBetween(LocalDate.now().minusDays(29), LocalDate.now().plusDays(1));
        String date[] = new String[30];
        for (int i = 0; i < date.length; i++) {
            date[i] = listDays.get(i).toString();
        }
        graphTraficKeywords.setDate(date);
        //calculate
        double val[] = new double[30];
        for (int i = 0; i < val.length; i++) {
            val[i] = calculateGraph(data, date[i]);
        }
        graphTraficKeywords.setValue(val);
        // searching max value
        double max = 0;
        for (int i = 0; i < val.length; i++) {
            if (val[i] > max) {
                max = val[i];
            }
        }
        graphTraficKeywords.setMaxValue((int)(max + 3));
        
        return graphTraficKeywords;
    }

    private double calculateGraph(List<RankTrackingLog> rankTrackingLogs, String date) {
//        System.out.println("date DB = " + rankTrackingLogs.get(0).getDate_time().toString().substring(0, 10));
//        System.out.println("Date search = "+date);
        double count = 0;
        double find = 0;
        for (int i = 0; i < rankTrackingLogs.size(); i++) {
//            System.out.println("date list 30 hari  = " + rankTrackingLogs.get(i).getDate_time().toString().substring(0, 10));
//            System.out.println("Date search = " + date);
//            System.out.println("=============================");
            if (rankTrackingLogs.get(i).getDate_time().substring(0, 10).equals(date)) {
                count += rankTrackingLogs.get(i).getRank();
                find++;
            }
        }
        if (find == 0) {
            return 0;            
        }
        
        return count / find;
    }
    
    private int minRank(List<RankTrackingLog> rankTrackingLogs) {
        
        int max = 0;
        for (int i = 0; i < rankTrackingLogs.size(); i++) {
            if (rankTrackingLogs.get(i).getRank() > max) {
                max = rankTrackingLogs.get(i).getRank();
            }
        }
        return max;
    }
    
    private double avgRank(List<RankTrackingLog> rankTrackingLogs) {
        double total = 0;
        for (int i = 0; i < rankTrackingLogs.size(); i++) {
            total += rankTrackingLogs.get(i).getRank();
        }
        
        return convertDouble(total / (double) rankTrackingLogs.size());
    }
    
    private int maxRank(List<RankTrackingLog> rankTrackingLogs) {
        int min = 10;
        for (int i = 0; i < rankTrackingLogs.size(); i++) {
            if (rankTrackingLogs.get(i).getRank() < min) {
                min = rankTrackingLogs.get(i).getRank();
            }
        }
        return min;
    }
    
    private double convertDouble(double d) {
        DecimalFormat df = new DecimalFormat("#.##");
        double z = Double.parseDouble(df.format(d));
        return z;
    }

    private WebPage updatePageRank(WebPage webPage, ArrayList<String> rank) throws java.text.ParseException {
        ArrayList<Integer> list = new ArrayList<>();
        int sum = 0;
        for (int i = 0; i < rank.size(); i++) {
            if (!rank.get(i).equals("unranked")) {
                list.add(Integer.parseInt(rank.get(i)));
                sum += Integer.parseInt(rank.get(i));
            }
        }
        webPage.setHighRank(Integer.toString(Collections.max(list)));
        webPage.setLowRank(Integer.toString(Collections.min(list)));
        webPage.setAvgRank(Integer.toString(sum / rank.size()));
        webPage.setLastUpdate(getCurentTime());
//        localConnection.updateWebPage(webPage);
        return webPage;
    }
    
    private Timestamp getCurentTime() {
        java.util.Date date = new java.util.Date();
        java.sql.Timestamp sqlTime = new java.sql.Timestamp(date.getTime());
        System.out.println("DATE TIME" + sqlTime);
        
        return sqlTime;
        
    }
    
    private String convertKeyword(String keyword) {
        return keyword.replace(" ", "+");
    }
    
    private String getCleanDomain(String st) {
        return st.substring(
                (st.indexOf(':') + 3),
                st.indexOf('/', st.indexOf(':') + 3));
        
    }
    
    private List<LocalDate> getDatesBetween(
            LocalDate startDate, LocalDate endDate) {
        long numOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate);
        return IntStream.iterate(0, i -> i + 1)
                .limit(numOfDaysBetween)
                .mapToObj(i -> startDate.plusDays(i))
                .collect(Collectors.toList());
    }
}
