/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.controller;

/**
 *
 * @author Centry
 */
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.datavisual.exception.ResourceNotFoundException;
import com.datavisual.model.RankTrackingKeyword;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.datavisual.repository.RankTrackingKeywordRepository;

@RestController
@RequestMapping("/api/keyword")
public class KeyWordController {

    @Autowired
    private RankTrackingKeywordRepository keyWordRepository;

    @GetMapping("/all")
    public List<RankTrackingKeyword> getAllKeyWords() {
        return keyWordRepository.findAll();
    }

    @GetMapping("/all/{id}")
    public List<RankTrackingKeyword> getAllKeyWordsByPageLinkId(@PathVariable(value = "id") Long keyWordId) {
//        return keyWordRepository.findByPageLinkId(keyWordId);
          return null;
    }

    @GetMapping("/{id}")
    public ResponseEntity<RankTrackingKeyword> getKeyWordById(@PathVariable(value = "id") Long keyWordId)
            throws ResourceNotFoundException {
        RankTrackingKeyword employee = keyWordRepository.findById(keyWordId)
                .orElseThrow(() -> new ResourceNotFoundException("KeyWord not found for this id :: " + keyWordId));
        return ResponseEntity.ok().body(employee);
    }

    @PostMapping("/add")
    public RankTrackingKeyword createKeyWord(@Valid @RequestBody RankTrackingKeyword keyWord) {
        return keyWordRepository.save(keyWord);
    }

//    @PostMapping(path = "/add") // Map ONLY POST Requests
//    public KeyWord addToKeyWord(@RequestBody KeyWord employee) {
//        return keyWordRepository.save(data);
//    }
    @PutMapping("/edit/{id}")
    public ResponseEntity<RankTrackingKeyword> updateKeyWord(@PathVariable(value = "id") Long keyWordId,
            @Valid @RequestBody RankTrackingKeyword keyWordData) throws ResourceNotFoundException {
        RankTrackingKeyword keyWord = keyWordRepository.findById(keyWordId)
                .orElseThrow(() -> new ResourceNotFoundException("KeyWord not found for this id :: " + keyWordId));

//        keyWord.setKeyWord(keyWordData.getKeyWord());
//        keyWord.setPageLinkId(keyWordData.getPageLinkId());
//        keyWord.setSearchRank(keyWordData.getSearchRank());

        final RankTrackingKeyword updatedKeyWord = keyWordRepository.save(keyWord);
        return ResponseEntity.ok(updatedKeyWord);
    }

    @DeleteMapping("/remove/{id}")
    public Map<String, Boolean> deleteKeyWord(@PathVariable(value = "id") Long keyWordId)
            throws ResourceNotFoundException {
        RankTrackingKeyword keyWord = keyWordRepository.findById(keyWordId)
                .orElseThrow(() -> new ResourceNotFoundException("KeyWord not found for this id :: " + keyWordId));

        keyWordRepository.delete(keyWord);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
