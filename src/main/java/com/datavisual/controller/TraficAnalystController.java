/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.controller;

import com.datavisual.model.Country;
import com.datavisual.model.GoogleConsole;
import com.datavisual.model.TraficAnalyst;
import com.datavisual.model.TraficAnalystPage;
import com.datavisual.model.TraficCountry;
import com.datavisual.model.TraficAnalystSumary;

import com.datavisual.model.TraficPage;
import com.datavisual.repository.GoogleConsoleRepository;
import com.datavisual.repository.TraficAnalystRepository;
import com.datavisual.repository.TraficCountryRepository;
import com.datavisual.repository.TraficPageRepository;
import com.google.common.collect.HashBiMap;
import com.sun.javafx.css.CalculatedValue;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.tomcat.jni.Pool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.tags.TransformTag;
import com.datavisual.model.RequestPage;
import com.datavisual.model.TraficAnalystPageKeyword;
import com.datavisual.repository.CountryListRepository;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author Centry
 */
@CrossOrigin
@RestController
@RequestMapping("/traficanalyst")
public class TraficAnalystController {

    @Autowired
    private TraficAnalystRepository traficAnalystRepository;
    @Autowired
    private TraficPageRepository traficPageRepository;
    @Autowired
    private TraficCountryRepository traficCountryRepository;
    @Autowired
    private GoogleConsoleRepository googleConsoleRepository;
    @Autowired
    private CountryListRepository countryListRepository;

    @GetMapping("/data")
    public List<TraficAnalyst> getAllData() {
        return traficAnalystRepository.getAllData();
    }

    @GetMapping("/trafic/all")
    public List<TraficAnalyst> getTrafic() {
        List<TraficAnalyst> traficAnalysts = traficAnalystRepository.getAllData();
        List<String> webPage = traficAnalystRepository.getPageList();
        List<TraficAnalyst> dataResult = new ArrayList<>();
        int found = 0;
        ArrayList<String> country = new ArrayList<>();
        ArrayList<String> device = new ArrayList<>();
        for (int i = 0; i < webPage.size(); i++) {

            for (int j = 0; j < traficAnalysts.size(); j++) {
                if (webPage.get(i).equals(traficAnalysts.get(j).getPage_link())) {
                    if (found == 0) {
                        dataResult.add(new TraficAnalyst(
                                traficAnalysts.get(j).getId(),
                                traficAnalysts.get(j).getAdd_date(),
                                traficAnalysts.get(j).getPage_link(),
                                traficAnalysts.get(j).getKeyword(),
                                traficAnalysts.get(j).getCountry(),
                                traficAnalysts.get(j).getDevice(),
                                traficAnalysts.get(j).getClick()));
                        country.add(traficAnalysts.get(j).getCountry());
                        device.add(traficAnalysts.get(j).getDevice());
                        found++;
                    } else {
                        dataResult.get(i).setAdd_date(traficAnalysts.get(j).getAdd_date());
                        dataResult.get(i).setCountry(calculateCountry(country, traficAnalysts.get(j).getCountry()));
                        dataResult.get(i).setDevice(calculateDevice(device, traficAnalysts.get(j).getDevice()));
                        dataResult.get(i).setClick(
                                dataResult.get(i).getClick() + traficAnalysts.get(j).getClick()
                        );
                    }
                }
            }
            found = 0;
            country.clear();
            device.clear();

        }

        return dataResult;
    }

    @PostMapping("trafic/page/detail")
    public List<GoogleConsole> getTraficPage(@RequestBody RequestPage req) {
//            return  googleConsoleRepository.getPageTrafic(req.getPagelink());
//        System.out.println("REQ Body = "+req.toString());
        List<GoogleConsole> data = googleConsoleRepository.getPageTrafic(req.getPageLink(), req.getDomainId());

        //getName of coutry and limit comma
        for (int i = 0; i < data.size(); i++) {
            data.get(i).setCountry(
                    getCountryName(data.get(i).getCountry())
            );
            data.get(i).setCtr(
                    convertDouble(data.get(i).getCtr())
            );
            data.get(i).setPosition(
                    convertDouble(data.get(i).getPosition())
            );
        }

        return data;
    }

    //getCoutry()
    private String getCountryName(String code) {
        Country country = countryListRepository.getCountryName(code.toUpperCase());

        return country.getCountryname();
    }

    @PostMapping("/trafic/page/detail/keyword")
    public List<TraficAnalystPageKeyword> getKeywordTrafic(@RequestBody RequestPage req) {
        List<GoogleConsole> rawData = googleConsoleRepository.getPageTrafic(req.getPageLink(), req.getDomainId());
        List<String> keyword = googleConsoleRepository.getKeywordFromPageTrafic(req.getPageLink(), req.getDomainId());
        List<TraficAnalystPageKeyword> pageKeyword = new ArrayList<>();

        for (int i = 0; i < keyword.size(); i++) {
            TraficAnalystPageKeyword traficAnalystPageKeyword = new TraficAnalystPageKeyword();
            traficAnalystPageKeyword.setKeyword(keyword.get(i));
            traficAnalystPageKeyword.setTrafic(
                    countTrafickKeyword(rawData, keyword.get(i))
            );
            traficAnalystPageKeyword.setClick(
                    countClickKeyword(rawData, keyword.get(i))
            );
            traficAnalystPageKeyword.setCtr(
                    convertDouble(
                            countCTRKeyword(rawData, keyword.get(i))
                    )
            );
            traficAnalystPageKeyword.setImpresion(
                    convertDouble(
                            countImpresionKeyword(rawData, keyword.get(i))
                    )
            );
            traficAnalystPageKeyword.setPosition(
                    convertDouble(
                            countPositionKeyword(rawData, keyword.get(i))
                    )
            );

            pageKeyword.add(traficAnalystPageKeyword);
        }
        return pageKeyword;
    }

    private int countTrafickKeyword(List<GoogleConsole> rawData, String keyword) {
        int count = 0;
        for (int i = 0; i < rawData.size(); i++) {
            if (rawData.get(i).getKey().equals(keyword)) {
                count++;
            }
        }
        return count;
    }

    private int countClickKeyword(List<GoogleConsole> rawData, String keyword) {
        int count = 0;
        for (int i = 0; i < rawData.size(); i++) {
            if (rawData.get(i).getKey().equals(keyword)) {
                count += rawData.get(i).getClick();
            }
        }
        return count;
    }

    private double countCTRKeyword(List<GoogleConsole> rawData, String keyword) {
        double count = 0;
        double match = 0;
        for (int i = 0; i < rawData.size(); i++) {
            if (rawData.get(i).getKey().equals(keyword)) {
                count += rawData.get(i).getCtr();
                match++;
            }
        }
        return count / match;
    }

    private double countImpresionKeyword(List<GoogleConsole> rawData, String keyword) {
        double count = 0;
        double match = 0;
        for (int i = 0; i < rawData.size(); i++) {
            if (rawData.get(i).getKey().equals(keyword)) {
                count += rawData.get(i).getImpresion();
                match++;
            }
        }
        return count / match;
    }

    private double countPositionKeyword(List<GoogleConsole> rawData, String keyword) {
        double count = 0;
        double match = 0;
        for (int i = 0; i < rawData.size(); i++) {
            if (rawData.get(i).getKey().equals(keyword)) {
                count += rawData.get(i).getPosition();
                match++;
            }
        }
        return count / match;
    }
    @GetMapping("/trafic/page/device/{idDomain}")
    public Map<String,Integer> getTraficByDevice(@PathVariable (name = "idDomain")Long idDomain){
        Map<String, Integer> map  = new HashMap<>();
        map.put("mobile", googleConsoleRepository.getTotalMobileDevice(idDomain));
        map.put("desktop", googleConsoleRepository.getTotalDesktopDevice(idDomain));
        
        return map;
    }

    @GetMapping("/trafic/page/sumary/{idDomain}")
    public TraficAnalystSumary getSumary(@PathVariable(name = "idDomain") Long idDomain) {
        TraficAnalystSumary traficAnalystSumary = new TraficAnalystSumary();
        List<GoogleConsole> dataLast30Days = googleConsoleRepository.getListDataForSumary(idDomain);

        List<LocalDate> listDays = getDatesBetween(LocalDate.now().minusDays(29), LocalDate.now().plusDays(1));
        String dates[] = new String[30];
        //adding date
        for (int i = 0; i < dates.length; i++) {
            dates[i] = listDays.get(i).toString();
        }
        traficAnalystSumary.setDates(dates);
        //add trafic;        
        int trafic[] = new int[30];
        for (int i = 0; i < trafic.length; i++) {
            trafic[i] = getTraficByDate(dataLast30Days, dates[i]);
        }
        traficAnalystSumary.setTrafic(trafic);
        // add Click
        int click[] = new int[30];
        for (int i = 0; i < click.length; i++) {
            click[i] = getClickByDate(dataLast30Days, dates[i]);
        }
        traficAnalystSumary.setClick(click);
        // add CTR
        int ctr[] = new int[30];
        for (int i = 0; i < ctr.length; i++) {
            ctr[i] = getCtrByDate(dataLast30Days, dates[i]);
        }
        traficAnalystSumary.setCtr(ctr);
        //set vax value;
        traficAnalystSumary.setMaxValue(countMaxValues(click, trafic));
        traficAnalystSumary.setAverageTrafic(averageTraficSumary(trafic));
        traficAnalystSumary.setTotalVisit(getTotalVisit(click));
        traficAnalystSumary.setTotalTrafic(getTotalTrafic(trafic));
        traficAnalystSumary.setAverageCTR(getAverageCTR(ctr));
        traficAnalystSumary.setTotalKeyword(getTotalKeyword(dataLast30Days));
        traficAnalystSumary.setAveragePosition(getAveragePosition(dataLast30Days));
        return traficAnalystSumary;
    }

    private int getTotalVisit(int click[]) {
        int count = 0;
        for (int i = 0; i < click.length; i++) {
            count += click[i];
        }
        return count;
    }
    private double getAveragePosition(List<GoogleConsole> googleConsoles){
        double count = 0;
        for (int i = 0; i < googleConsoles.size(); i++) {
            count += googleConsoles.get(i).getPosition();
        }
        return convertDouble(count / googleConsoles.size());
    }
    
    private int getTotalKeyword(List<GoogleConsole>  googleConsoles){
        Map<String, Integer> dataKeyword = new HashMap<>();
        
        for (int i = 0; i < googleConsoles.size(); i++) {
              dataKeyword.put(googleConsoles.get(i).getKey(), i);
        }
        return dataKeyword.size();
    }

    private int getTotalTrafic(int trafic[]) {
        int count = 0;
        for (int i = 0; i < trafic.length; i++) {
            count += trafic[i];
        }

        return count;
    }

    private int getAverageCTR(int ctr[]) {
        int count = 0;
        for (int i = 0; i < ctr.length; i++) {
            count += ctr[i];
        }
        return count / ctr.length;
    }
    

    private double averageTraficSumary(int trafic[]) {
        double count = 0;
        for (int i = 0; i < trafic.length; i++) {
            count += (double) trafic[i];
        }
        return convertDouble(count / ((double) trafic.length));
    }

    private int getTraficByDate(List<GoogleConsole> googleConsoles, String date) {
        int count = 0;
        for (int i = 0; i < googleConsoles.size(); i++) {
            if (googleConsoles.get(i).getDate().toString().equals(date)) {
                count++;
            }
        }
        return count;
    }

    private int getClickByDate(List<GoogleConsole> googleConsoles, String date) {
        int count = 0;
        for (int i = 0; i < googleConsoles.size(); i++) {
            if (googleConsoles.get(i).getDate().toString().equals(date)) {
                count += googleConsoles.get(i).getClick();
            }
        }
        return count;
    }

    private int getCtrByDate(List<GoogleConsole> googleConsoles, String date) {
        double count = 0;
        double match = 0;
        for (int i = 0; i < googleConsoles.size(); i++) {
            if (googleConsoles.get(i).getDate().toString().equals(date)) {
                match++;
                count += googleConsoles.get(i).getCtr();
            }
        }

        return (int) ((count / match) * 100);
    }

    private int countMaxValues(int click[], int trafic[]) {
        int max = 0;
        for (int i = 0; i < click.length; i++) {
            if (click[i] > max) {
                max = click[i];
            }
        }
        for (int i = 0; i < trafic.length; i++) {
            if (trafic[i] > max) {
                max = trafic[i];
            }
        }

        return max + 5;
    }

    private List<LocalDate> getDatesBetween(
            LocalDate startDate, LocalDate endDate) {
        long numOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate);
        return IntStream.iterate(0, i -> i + 1)
                .limit(numOfDaysBetween)
                .mapToObj(i -> startDate.plusDays(i))
                .collect(Collectors.toList());
    }
    
    

    @GetMapping("/trafic/page/{idDomain}")
    public List<TraficAnalystPage> getTraficByDate(@PathVariable(name = "idDomain") Long idDomain) {
        List<GoogleConsole> rawData = googleConsoleRepository.getAllDataPageByDomainId(idDomain);
        List<String> dataPage = googleConsoleRepository.getUniqueDataPage(idDomain);
        List<TraficAnalystPage> traficPage = new ArrayList<>();

        for (int i = 0; i < dataPage.size(); i++) {
            TraficAnalystPage traficAnalystPage = new TraficAnalystPage();

            traficAnalystPage.setPage(dataPage.get(i));
            traficAnalystPage.setTrafic(
                    getTotalTrafic(rawData, dataPage.get(i))
            );
            traficAnalystPage.setClick(
                    getTotalClick(rawData, dataPage.get(i))
            );
            traficAnalystPage.setCtr(
                    convertDouble(
                            getTotalCtr(rawData, dataPage.get(i))
                    )
            );
            traficAnalystPage.setImpresion(
                    convertDouble(
                            getTotalImpresion(rawData, dataPage.get(i))
                    )
            );
            traficAnalystPage.setPosition(
                    convertDouble(
                            getAvgPosition(rawData, dataPage.get(i))
                    )
            );

            traficPage.add(traficAnalystPage);
        }

        return traficPage;
    }

    private int getTotalTrafic(List<GoogleConsole> data, String page) {
        int count = 0;
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getLink().equals(page)) {
                count++;
            }
        }

        return count;
    }

    private int getTotalClick(List<GoogleConsole> data, String page) {
        int count = 0;
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getLink().equals(page)) {
                count += data.get(i).getClick();
            }
        }
        return count;
    }

    private double getTotalCtr(List<GoogleConsole> data, String page) {
        double count = 0;
        double total = 0;
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getLink().equals(page)) {
                total++;
                count += data.get(i).getCtr();
            }
        }
        return count / total;
    }

    private double getTotalImpresion(List<GoogleConsole> data, String page) {
        double count = 0;
        double total = 0;
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getLink().equals(page)) {
                total++;
                count += data.get(i).getImpresion();
            }
        }
        return count / total;
    }

    private double getAvgPosition(List<GoogleConsole> data, String page) {
        double count = 0;
        double total = 0;
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getLink().equals(page)) {
                total++;
                count += data.get(i).getPosition();
            }
        }
        return count / total;
    }

    private double convertDouble(double d) {
        DecimalFormat df = new DecimalFormat("#.#####");
        double z = Double.parseDouble(df.format(d));
        return z;
    }

    @GetMapping("/trafic/country")
    public List<TraficCountry> getTrafickCountry() {
        return traficCountryRepository.getTraficCountry();

    }

    private String calculateCountry(ArrayList<String> country, String data) {
        String str = "";
        boolean status = false;
        for (int i = 0; i < country.size(); i++) {
            if (data.equals(country.get(i))) {
                status = true;
                break;
            }
        }
        if (status == true) {
            for (int i = 0; i < country.size(); i++) {
                str = str + country.get(i);
                if (i != (country.size() - 1)) {
                    str += ", ";
                }

            }
        } else {
            for (int i = 0; i < country.size(); i++) {
                str = str + country.get(i) + ", ";
            }
            str = str + data;
        }
        return str;
    }

    private String calculateDevice(ArrayList<String> device, String data) {
        String str = "";
        boolean status = false;
        for (int i = 0; i < device.size(); i++) {
            if (data.equals(device.get(i))) {
                status = true;
                break;
            }
        }
        if (status == true) {
            for (int i = 0; i < device.size(); i++) {
                str = str + device.get(i);
                if (i != (device.size() - 1)) {
                    str += ", ";
                }

            }
        } else {
            for (int i = 0; i < device.size(); i++) {
                str = str + device.get(i) + ", ";
            }
            str = str + data;
        }
        return str;
    }
}
