/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.repository;

import com.datavisual.model.TraficAnalyst;
import com.datavisual.model.TraficPage;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TraficAnalystRepository extends JpaRepository<TraficAnalyst, Long> {

    @Query(value = "SELECT  `id`, `add_date`, `page_link`, (SELECT countryname FROM country WHERE `countrycode` = `country`)AS `country`, `device`,`click`, `keyword`  FROM google_console ORDER BY `add_date` ASC", nativeQuery = true)
    List<TraficAnalyst> getAllData();
    
    @Query(value ="SELECT DISTINCT page_link FROM google_console",nativeQuery = true)
    List<String> getPageList();
    

    
}
