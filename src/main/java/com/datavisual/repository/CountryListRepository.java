/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.repository;

import com.datavisual.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface CountryListRepository extends JpaRepository<Country, String>{
    
    @Query(value = "select * from country where countrycode = ?1",nativeQuery = true)
    Country getCountryName(String code);
    
}
