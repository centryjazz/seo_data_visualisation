/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.repository;

import com.datavisual.model.KeyWordPlanerMonthSearch;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Centry
 */
@Repository
public interface KeyWordPlanerMonthSearchRepository extends JpaRepository<KeyWordPlanerMonthSearch, Long>{
    
    List<KeyWordPlanerMonthSearch> findById(long id);
    
    @Query(value = "SELECT * FROM keyword_history WHERE keyword_planer = ?1",nativeQuery = true)
    List<KeyWordPlanerMonthSearch> getDataByKeyword(long keyword);
    
    @Query(value = "SELECT * FROM keyword_history WHERE keyword_planer = ?1 ORDER BY year ASC, month ASC",nativeQuery = true)
    List<KeyWordPlanerMonthSearch> getDataByKeywordASC(long keyword);
}
