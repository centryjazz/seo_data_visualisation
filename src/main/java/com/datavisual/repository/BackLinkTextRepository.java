/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.repository;

import com.datavisual.model.BackLinkTarget;
import com.datavisual.model.BackLinkText;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Centry
 */
@Repository
public interface BackLinkTextRepository extends JpaRepository<BackLinkText, Long>{
    @Query(value = "SELECT * FROM backlink_text WHERE domain_id = ?1",nativeQuery = true)
    List<BackLinkText> getBackLinkText(long idDomain);
    
    @Query(value = "SELECT COUNT(keyword) FROM backlink_text WHERE domain_id = ?1",nativeQuery = true)
    int getTotalKeyword(long idDomain);
    //total linked from keyword
}
