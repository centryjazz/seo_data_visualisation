/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.repository;

import com.datavisual.model.RankTrackingLog;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Centry

*/
@Repository
public interface RankTrackingLogRepository extends JpaRepository<RankTrackingLog, Long>{
    @Query(value = "SELECT * FROM rank_tracking_log WHERE keyword_id = ?1 ORDER BY date_time DESC ",nativeQuery = true)
    List<RankTrackingLog> getDataByKeywordId (long keywordId);
    
    @Query(value = "SELECT * FROM rank_tracking_log WHERE page = ?1 AND keyword_id = ?2", nativeQuery = true)
    List<RankTrackingLog> getGraphForDataTraficKeyword(String page, long id);
}
