/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.repository;

import com.datavisual.model.Domain;
import com.datavisual.model.RankTrackingKeyword;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Centry
 */
@Repository
public interface DomainRepository extends JpaRepository<Domain, Long>{
   
   @Query(value = "select * from  domain where id = ?1",nativeQuery = true)  
   Domain getDomainDataById(long id);
   
   @Query(value = "select * from  domain where id = ?1",nativeQuery = true)  
   List<Domain> getDomainsDataById(long id);
   
   List<Domain> findByUser(Long id);
}
