/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.repository;

import com.datavisual.model.KeyWordResearch;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Centry
 */
@Repository
public interface KeyWordResearchRepository extends JpaRepository<KeyWordResearch, Long>{
    
    @Query(value = "SELECT id, add_date, keyword, click, ctr, impresion, `position` FROM google_console ORDER BY add_date ASC, keyword ASC, click DESC", nativeQuery = true)
    List<KeyWordResearch> getAllDataKeywordResearch();
    
    @Query(value = "select distinct keyword from google_console", nativeQuery = true)
    List<String> getKeyword();
}
