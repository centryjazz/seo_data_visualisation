/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.repository;

import com.datavisual.model.AuthLogin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Centry
 */
@Repository
public interface AuthLoginRepository extends JpaRepository<AuthLogin, Long>{
        
    @Query(value = "select * from user where username = ?1",nativeQuery = true)
    AuthLogin getAuth(String username);
    
    @Query(value = "SELECT COUNT(id) FROM USER WHERE username = ?1",nativeQuery = true)
    int checkUser(String username);
    
    
}
