/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.repository;

import com.datavisual.model.RankTrackingKeyword;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;


/**
 *
 * @author Centry
 */
@Repository
public interface RankTrackingKeywordRepository extends JpaRepository<RankTrackingKeyword, Long> {

    @Query(value = "select * from rank_tracking_keyword where domain_id = ?1",nativeQuery = true)
    List<RankTrackingKeyword> getKeywordListByDomain(long id);
    
    @Query(value = "select * from rank_tracking_keyword where domain_id = ?1 and max_rank > 0",nativeQuery = true)
    List<RankTrackingKeyword> getKeywordListByDomainWithValueCheck(long id);
    
    
    @Query(value = "SELECT COUNT(id) FROM rank_tracking_keyword WHERE max_rank <= 0 and domain_id = ?1",nativeQuery = true)
    int getUnrankedKeyword(long id);
}
