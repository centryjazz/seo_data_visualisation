/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.datavisual.model.Log;
import org.springframework.stereotype.Repository;
/**
 *
 * @author Centry
 */ 
@Repository
public interface LogRepository extends JpaRepository<Log, Long>{
    
}
