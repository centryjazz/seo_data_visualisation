/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.repository;

import com.datavisual.model.KeyWordPlaner;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Centry
 */
@Repository
public interface KeyWordPlannerRepository extends JpaRepository<KeyWordPlaner, Long>{
    
    @Query(value = "select keyword from keyword_planner where domain_id = ?1 ORDER BY id ASC",nativeQuery = true)
    List<String> getAllKeyword(long idDomain);
    
    @Query(value = "select * from keyword_planner where domain_id = ?1 ORDER BY id ASC",nativeQuery = true)
    List<KeyWordPlaner> getAll(long idDomain);
    
    @Query(value = "SELECT id FROM `keyword_planner` ORDER BY id ASC",nativeQuery = true)
    List<Long> getAllId();
    
   @Query(value ="SELECT DISTINCT `keyword` FROM google_console WHERE keyword LIKE %?1%",nativeQuery = true)
   List<String> getSameKeyword(String keyword);
   
   @Query(value = "SELECT AVG(`position`) FROM google_console WHERE keyword = ?1",nativeQuery = true)
   double getAVGKeyWordPosition(String keyword);
   
   @Query(value = "SELECT AVG(`impresion`) FROM google_console WHERE keyword = ?1",nativeQuery = true)
   double getAVGKeyWordImpresion(String keyword);
}
