/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.repository;
import com.datavisual.model.WebPage;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Centry
 */
@Repository
public interface WebPageRepository extends JpaRepository<WebPage, Long>{
    
    @Query(value = "select * from web_page where id = ?1",nativeQuery = true)
    List<WebPage> findByWebPageId(long pageId);
    
}
