/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.repository;

import com.datavisual.model.BackLinkSite;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Centry
 */
@Repository
public interface BackLinkSiteRepository extends JpaRepository<BackLinkSite, Long>{
    @Query(value = "SELECT * FROM backlink_site WHERE domain_id = ?1",nativeQuery = true)
    List<BackLinkSite> getBackLinkSite(long idDomain);
    
    @Query(value = "SELECT SUM(linking_page) FROM backlink_site where domain_id = ?1",nativeQuery = true)
    int getTotalBackLink(long idDomain);
    // total backLink
    @Query(value = "SELECT COUNT(url) FROM backlink_site where domain_id = ?1",nativeQuery = true)
    int getTotalSite(long idDomain);       
    // total site
    

}
