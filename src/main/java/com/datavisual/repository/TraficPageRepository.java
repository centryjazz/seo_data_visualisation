/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.repository;

import com.datavisual.model.TraficPage;
import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Centry
 */
@Repository
public interface TraficPageRepository extends JpaRepository<TraficPage, Long> {

    @Query(value = "SELECT id, add_date, page_link, COUNT(DISTINCT add_date, page_link) AS 'trafic' from google_console GROUP BY page_link ORDER BY add_date ASC", nativeQuery = true)
    List<TraficPage> getTraficPage();
}
