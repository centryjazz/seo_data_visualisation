/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.repository;

import com.datavisual.model.GoogleConsole;
import com.datavisual.model.KeyWordResearch;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Centry
 */
@Repository
public interface GoogleConsoleRepository  extends JpaRepository<GoogleConsole, Long>{
    
    @Query(value = "SELECT add_date, keyword, click, ctr, impresion, `position` FROM google_console ORDER BY add_date ASC, keyword ASC, click DESC", nativeQuery = true)
    List<KeyWordResearch> getAllDataKeywordResearch();
    
    @Query(value = "select distinct keyword from google_console", nativeQuery = true)
    List<String> getKeyword();
    
    @Query(value = "SELECT * FROM google_console WHERE domain_id = ?1",nativeQuery = true)
    List<GoogleConsole> getAllDataPageByDomainId(long domainId);
    
    @Query(value = "SELECT DISTINCT page_link FROM google_console WHERE domain_id =?1",nativeQuery = true)
    List<String> getUniqueDataPage(long domainId);
    
    @Query(value = "SELECT * FROM google_console WHERE page_link = ?1 AND domain_id = ?2 ORDER BY add_date DESC", nativeQuery = true)
    List<GoogleConsole> getPageTrafic(@Param("page")String page, @Param("domain") long domain_id);
    
//    List<GoogleConsole> findByPage_linkAndDomain_id(String pageLink,long domain_id);
    @Query(value = "SELECT DISTINCT keyword FROM google_console WHERE page_link = ?1 AND domain_id = ?2 ORDER BY add_date DESC",nativeQuery = true)
    List<String> getKeywordFromPageTrafic(String page, long domain);
    
    @Query(value = "SELECT * FROM google_console WHERE add_date BETWEEN NOW() - INTERVAL 30 DAY AND NOW() AND domain_id = ?1 ORDER BY add_date DESC",nativeQuery = true)
    List<GoogleConsole> getListDataForSumary(long idDomain);
    
    @Query(value = "Select sum(position)/count(id) from google_console where domain_id = ?1",nativeQuery = true)
    double getRank(Long idDomain);
    
    @Query(value = "Select count(id) from google_console WHERE add_date LIKE curdate() AND domain_id = ?1",nativeQuery = true)
    int getTodayTrafic(Long idDomain);
    
    @Query(value = "Select sum(linking_page) from backlink_site where domain_id= ?1",nativeQuery = true)
    int getTotalBacklink(Long idDomain);
    
    @Query(value = "Select count(id) from google_console where domain_id= ?1",nativeQuery = true)          
    int getTotalTrafic(Long idDomain);
    
    @Query(value = "Select sum(click) from google_console where domain_id= ?1",nativeQuery = true)          
    int getTotalClick(Long idDomain);
    
    @Query(value = "Select Sum(ctr)/Sum(impresion) from google_console where domain_id= ?1",nativeQuery = true)          
    double getTotalCTR(Long idDomain);
    
    @Query(value = "Select count(distinct keyword) from google_console where domain_id= ?1",nativeQuery = true)          
    int getTotalKeyword(Long idDomain);
    
    
    @Query(value = "SELECT COUNT(device) FROM google_console WHERE device = 'MOBILE' AND domain_id= ?1",nativeQuery = true)          
    int getTotalMobileDevice(Long idDomain);
    
    @Query(value = "SELECT COUNT(device) FROM google_console WHERE device = 'DESKTOP' AND domain_id= ?1",nativeQuery = true)          
    int getTotalDesktopDevice(Long idDomain);
    
}
