/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.repository;

import com.datavisual.model.TraficCountry;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Centry
 */
@Repository
public interface TraficCountryRepository extends JpaRepository<TraficCountry, String>{
    
    @Query(value = "SELECT (SELECT countryname FROM country WHERE `countrycode` = `country`) as 'country', COUNT(country)as 'trafic' FROM google_console group by country order by trafic DESC",nativeQuery = true)
    List<TraficCountry> getTraficCountry();
}
