/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.repository;

import com.datavisual.model.BackLinkSite;
import com.datavisual.model.BackLinkTarget;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Centry
 */
@Repository
public interface BackLinkTargetRepository extends JpaRepository<BackLinkTarget, Long>{
    @Query(value = "SELECT * FROM backlink_target WHERE domain_id = ?1",nativeQuery = true)
    List<BackLinkTarget> getBackLinkTarget(long idDomain);
    
    @Query(value = "SELECT COUNT(target_page) FROM backlink_target WHERE domain_id = ?1",nativeQuery = true)
    int getTotalPageLinked(long idDomain);
    //total page linked
    

}
