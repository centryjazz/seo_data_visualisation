package com.datavisual;

import com.datavisual.external.model.RankTrackingResult;
import com.datavisual.external.RankTracking;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.parser.ParseException;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import org.json.simple.parser.ParseException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
//import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication()
//@EnableJpaRepositories("com.zaki.datavisual.repository")
//@EntityScan("com.zaki.datavisual.model")
@EnableSwagger2
public class DatavisualApplication {

    public static void main(String[] args) {
        SpringApplication.run(DatavisualApplication.class, args);
//        try {
//            ArrayList<RankTrackingResult> rankTrackingResults = new RankTracking().getData("mysql",1);
//        } catch (ParseException ex) {
//            Logger.getLogger(DatavisualApplication.class.getName()).log(Level.SEVERE, null, ex);
//            ex.printStackTrace();
//        }
    }

    @Bean
    public Docket swaggerConfiguration() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .paths(PathSelectors.any())
                .apis(RequestHandlerSelectors.any())
                .build();
    }
}
