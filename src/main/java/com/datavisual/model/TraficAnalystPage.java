/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Centry
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TraficAnalystPage {
    private String page;
    private int trafic;
    private int click;
    private double ctr;
    private double impresion;
    private double position;
}
