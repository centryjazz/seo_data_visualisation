/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Centry
 */
@Data
@AllArgsConstructor
@NoArgsConstructor

public class BackLinkSumary {
    private int totalBackLink;
    private int totalSite;
    private int totalTargetPage;
    private int totalBackLinkKeyword;
    
    
}

