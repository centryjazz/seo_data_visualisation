/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.model;

import java.util.ArrayList;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Centry
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class KeywordResearchSumary {
    
    private String keyword;
    private double avgClick;
    private double avgCtr;
    private double avgImpresion;
    private double avgPosition;
    private double maxImpresion;
    private double maxCtr;
    private double maxPosition;
    private double totalClick;
    private double totalCtr;
    private double totalImpresion;

}
