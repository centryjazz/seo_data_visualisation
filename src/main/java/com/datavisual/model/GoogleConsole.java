/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.model;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Centry
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "google_console")
public class GoogleConsole {
   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   private long id;
      
   @Column(name = "add_date", nullable = false)
   private Date date;
   
   @Column(name = "page_link", nullable = false)
   private String link;
   
   @Column(name = "keyword", nullable = false)
   private String key;
   
   @Column(name = "country", nullable = false)
   private String country;
   
   @Column(name = "device", nullable = false)
   private String device;
   
   @Column(name = "click", nullable = false)
   private double click;
   
   @Column(name = "ctr", nullable = false)
   private double ctr;
   
   @Column(name = "impresion", nullable = false)
   private double impresion;
   
   @Column(name = "position", nullable = false)
   private double position;
   
   @Column(name = "domain_id", nullable = false)
   private long domain;
          
}
