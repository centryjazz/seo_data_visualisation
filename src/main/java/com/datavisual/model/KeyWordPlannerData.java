/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.model;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@AllArgsConstructor
@NoArgsConstructor
public class KeyWordPlannerData {
    private KeyWordPlaner keyWordPlaner;
    private List<KeyWordPlanerMonthSearch> keyWordPlanerMonthSearch;
}
