/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.model;

import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Centry
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TraficAnalyst {
    @Id
    private long id;
    private Date add_date;
    private String page_link;
    private String keyword;
    private String country;
    private String device;
    private double click;
}
