/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.model;

//import java.sql.Date;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
//import lombok.CustomLog;
import lombok.Data;
//import lombok.Generated;
import lombok.NoArgsConstructor;

/**
 *
 * @author Centry
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "web_page")
public class WebPage {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "title", nullable = false)
    private String title;
    @Column(name = "link", nullable = false)
    private String link;
    @Column(name = "high_rank", nullable = false)
    private String highRank;
    @Column(name = "avg_rank", nullable = false)
    private String avgRank;
    @Column(name = "low_rank", nullable = false)
    private String lowRank;
    @Column(name = "last_update", nullable = false)
    private Timestamp lastUpdate;
    @Column(name = "status", nullable = false)
    private String status;
    @Column(name = "domain", nullable = false)
    private String domain;
}
