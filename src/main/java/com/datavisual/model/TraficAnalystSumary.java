/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 *
 * @author Centry
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TraficAnalystSumary {
    private int maxValue;
    private String dates[];
    private int trafic[];
    private int click[];
    private int ctr[];
    private double averageTrafic;
    private int totalVisit;
    private int totalTrafic;
    private int averageCTR;
    private int totalKeyword;
    private double averagePosition;
}
