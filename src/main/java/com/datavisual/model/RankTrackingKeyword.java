package com.datavisual.model;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "rank_tracking_keyword")
public class RankTrackingKeyword {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String keyword;
    private int max_rank;
    private double avg_rank;
    private int min_rank;
    private String last_update;
    private long domain_id;

}
