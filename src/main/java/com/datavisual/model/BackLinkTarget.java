/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.model;

import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Centry
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "backlink_target")
public class BackLinkTarget {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String target_page;
    private int incoming_link;
    private int linking_site;
    private long domain_id;
    
}
