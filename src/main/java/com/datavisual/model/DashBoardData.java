/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class DashBoardData {
    private double rank;
    private int todayTafic;
    private int totalBackLink;
    private int totalTrafic;
    private int totalClick;
    private double totalCTR;
    private int totalKeyword;
}
