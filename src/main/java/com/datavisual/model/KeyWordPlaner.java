/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datavisual.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Centry
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "keyword_planner")
public class KeyWordPlaner {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String keyword;
    private double cpc;
    private int volume;
    private double competition;
    private int allinsearch;
    private String related_search;
    private long domain_id;
    
}
